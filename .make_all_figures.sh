#!/bin/bash

for py_file in $(find figures -name *.py)
do
    python $py_file
done
