# Reproduction code for Asymmetric node placement in fiber-based quantum networks

This repository holds the code required to reproduce the results in the paper
[Asymmetric node placement in fiber-based quantum networks](https://arxiv.org/abs/2305.09635) by
Guus Avis, Robert Knegjens, Anders S. Sørensen and Stephanie Wehner,
arXiv:2305.09635.
Additionally, it holds the raw simulation data produced using the simulation code included here that was used to create the figures in the paper, along with additional data not presented in the paper.


# Getting the code to work

## Installing requirements

The code included in this repository is Python code and has been verified to work with Python version 3.8.18.
In order to use the code, a number of Python packages needs to be installed.
Which packages these are is detailed in `requirements.txt`.
They can be installed automatically on a linux system by typing
```
make requirements
```
in a Linux terminal while in the root directory of this repository and hitting enter.
It is recommended to create a virtual environment before doing so.

One of the required packages is [NetSquid](https://netsquid.org/), a quantum-network simulator.
At the time of writing, NetSquid can only be obtained by downloading it from the NetSquid PyPI server, which requires authentication with NetSquid credentials.
Such an account can be created using the NetSquid website (see "Get NetSquid").
When doing `make requirements`, authentication with the correct server is automatically taken care of.
This, however, only works if the script has access to the NetSquid username and password.
If you want to use the automated command, before doing `make requirements`, you should set the environment variables `NETSQUIDPYPI_USER` and `NETSQUIDPYPI_PWD`.
Let `<username>` be your NetSquid user name and let `<pwd>` be your NetSquid password.
Then, you can set up the variables using the following two commands:
```
export NETSQUIDPYPI_USER=<username>
```
```
export NETSQUIDPYPI_PWD=<pwd>
```
Note that these variables are reset when the terminal is closed; they can be set permanently by adding these lines in your `~/.bashrc` file (when adding them to .bashrc, note that you either need to source that file or restart your terminal).

Some of the required packages are so-called NetSquid snippets, i.e., user-contributed libraries to be used with NetSquid.
When the username and password are set, `make requirements` will automatically download the required snippets from the NetSquid PyPI server.
However, unlike NetSquid itself, these libraries are open source and can also be accessed directly as gitlab repositories.
Information about snippets can be found [here](https://netsquid.org/snippets/), the snippets themselves can be found on gitlab [here](https://gitlab.com/softwarequtech/netsquid-snippets).


## Setting the Python path

In order to use the code, Python needs to know how to find the module holding it.
This can be realized by adding the root directory to the `PYTHONPATH` environment variable.
One way of doing this is by running this command while in the root directory of this repository:
```
export PYTHONPATH=${PYTHONPATH}:${PWD}
```
Note that this variable is also reset at the end of your session, to set it more permanently the variable can be exported in `~/.bashrc`, but in that case make sure to replace `${PWD}` in the command (which points the system to the current working directory) by the full path to the root directory of the repository.


## Testing installation

To test whether the code has been succesfully installed and runs correctly, you can execute
```
make tests
```
Note that the tests will run correctly even if the `PYTHONPATH` has not been correctly set, as the test suite is told explicitly which directory the code lives in.
If `make tests` runs successfully but running tests manually from the root folder (e.g., `pytest tests/test_simulate.py`) results in a `ModuleNotFoundError`, probably `PYTHONPATH` has not been properly exported.


# Accessing results

## Reproducing the paper figures

The figures included in the paper can be reproduced by, from the root directory, first running
```
make process
```
and then
```
make figures
```

The figures are created using Python scripts included in the `figures` folder (they create figures and then save them in the `figures` folder in a PNG format).
The only thing that `make figures` does is running all these files one by one.
If you only want to produce one specific figure, just run the corresponding Python file.
The figures that are based on simulations of quantum-repeater chains require data to plot.
The raw data that was produced for the paper is included here in the `data` folder.
Before the data can be plotted, it first needs to be processed.
This is done using the Python script `data/process_data.py`.
The only thing that `make process` does, is executing this script.
If you only want to produce a figure that is not produced by `figures/repeater_chain.py`, there is no need to process the data first.

Data processing can take 20-30 minutes.
Furthermore, `figures/compare_anlaytical_numerical_visibility.py` requires performing numerical integration for each data point and can take up to a few minutes.
Otherwise, all figure-producing scripts should finish within a few seconds.


## data

The folder `data` holds raw simulation data that was produced using `asymmetry_code/repeater_simulation/simulate.py`.
It is this data that is used to produce the repeater-chain figures included in the paper using `figures/repeater_chain.py`.
Additionally, it includes the YAML files used as input for the simulation; these files specify the configuration of the simulated repeater chain (e.g., its length, asymmetry and number of nodes) and how parameters are varied.
For more information about how the simulation script, the YAML configuration files and raw data work, see below.
There are two subfolders, for the two different repeater-chain settings that were considered in the paper.
Each contains a corresponding YAML configuration file, and a `raw_data` folder that contains all the raw data produced by the simulations.

The folder `no_spooled_fiber` includes a scan over the chain asymmetry parameter.
The folder `spooled_fiber` also includes such a scan, only here the "extended-fiber method" has been used (defined in the paper).



## Extra simulation results

This repository also holds some data from repeater-chain simulations that are not shown in the paper.
These data sets show that the conclusions drawn in the paper still hold when the values of some of the parameters are changed.
The data can be found in the folder `data/alternative_data`; each subdirectory holds YAML configuration files and data where a single parameter has been changed compared to the scenario investigated in the paper, and the name of the folder describes the changed parameter value.
For a complete description of the parameters used in the simulation, see the included YAML files.
The `alternative_data` folder has its own makefile that can be used for automated data processing and figure creation.
The easiest way of doing this is by going into the `alternative_data` folder with a terminal, simply typing `make` and hitting enter.
Afterwards (this may take a bit of time), each of the subdirectories should contain a PNG file that compares results for using and not using the extended-fiber method.


# Using the code

## Midpoint asymmetry

The folder `asymmetry_code/midpoint_asymmetry` contains the code corresponding to the first part of the paper, where the effect of an asymmetric midpoint station is investigated.

The file `elementary_link.py` contains functions for the rate and fidelity of single-click and double-click entanglement generation when not accounting for any loss in interference visibility between the photons.

The file `dispersion.py` contains functions for how the interference visibility between photons is reduced due to asymmetry.
It includes results for both Gaussian and Lorentzian photons, and implements both analytically-obtained results and numerical results.


## Repeater simulation

The folder `asymmetry_code/repeater_simulation` contains code to simulate SWAP-ASAP quantum-repeater chains with asymmetric node placement.

The file `chain_builder.py` is to be used with the NetSquid snippet [netsquid-netconf](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-netconf).
It allows configuring (asymmetric) repeater chains parametrically from an input YAML file.

The file `simulate.py` runs a simulation of BB84 on a SWAP-ASAP quantum-repeater chain.
When running it, a YAML configuration file for the repeater chain needs to be provided, which should specify the parameters defined in `chain_builder.py`.
The name of the configuration file should be provided as a positional argument.
The optional keyword argument `-n`/`--n_runs` can be provided to specify the length of the raw secret key that should be produced in the simulation.
The optional keyword argument `--save_path` can be provided to set the directory where the raw data should be stored.
By adding the flag `--plot`, the data will be immediately plotted.
The flag `-v`/`--verbose` makes the script print its output.

For examples of how to write a configuration YAML file to be used with `chain_builder.py` and `simulate.py`, see the YAML files in the `data` folder.

Raw data is produced in the form of serialized pickle files, which are not human readable but can be imported using Python.
Once imported, they take the fofm of `RepchainDataFrameHolder` objects as defined in the NetSquid snippet [netsquid-simulationtools](https://gitlab.com/softwarequtech/netsquid-snippets/netsquid-simulationtools).
In order to process the raw data (to extract, e.g., secret-key rate), `process_qkd.py` can be used, which does not do much more than run processing functions imported from netsquid-simulationtools.
Run it from within the raw-data directory, or specify the directory using the `-p`/`--path` optional keyword argument, and add the `--plot` flag to immediately plot the processed data.
As a result, it saves both a `processed_data.pickle` file that contains all the combined raw data, and `output.csv` which is human readable (it can, e.g., be opened by spreadsheet software) and contains all the results of the processing.
Processed data can also be plotted at a later time using `plot_qkd.py`, which also offers some extra customization options for plots, such as comparing two datasets using the `-c` flag (run `plot_qkd.py -h` for help).
It requires the CSV output file produced by data processing as input.
