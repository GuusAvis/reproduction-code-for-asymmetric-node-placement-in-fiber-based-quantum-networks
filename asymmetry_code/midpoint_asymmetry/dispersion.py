import cmath
import time
from functools import partial

import numpy as np
from scipy.special import fresnel
import scipy


speed_of_light_in_vacuum = 299792458  # m / s, exact value (defines the metre)


def gaussian_photon_visibility_dispersion_correction(length_difference, time_sigma, frequency_mismatch=0.,
                                                     time_offset=0., dispersion_coefficient=17, dispersion_slope=0.056,
                                                     wavelength=1550):
    """Correction to visibility of two indistinguishable Gaussian photons due to dispersion in fiber.

    emitter 1 ---- fiber of length L1 ---- midpoint station ---- fiber of length L2 ---- emitter 2

    At the midpoint station, Hong-Ou-Mandel interference is performed.
    The visibility V of the interference will be 1 when the photons are perfectly indistinguishable when they arrive,
    which is the case when L1 = L2. However, when L1 = L2, both photons will be dispersed to different degrees.

    This function returns a first-order correction v_1, such that the visibility is approximately V = v_0 - v_1.
    Here, v_0 is the visibility if there were no dispersion.
    This correction is based on the assumption that the photon has a small width in frequency space, and thus a long
    width in time. If photon_width is too small, this correction will become large and also higher-order
    corrections may become important.

    This function assumes the photons have identical Gaussian wave functions
    (Gaussian both as a function of time as as a function of frequency,
    as the Fourier transform of a Gaussian is another Gaussian)
    up to a possible frequency mismatch and temporal shift.

    Parameters
    ----------
    length_difference : float
        Difference in lengths [km] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    time_sigma : float
        Standard deviation [ns] of the photon's temporal probability density function, that is,
        |phi(t)|^2 is proportional to exp(- (1 / 2) (t / time_sigma)^2).
    frequency_mismatch : float (optional)
        Difference between central frequencies of the two Gaussian photons [GHz].
        Ideally this would be zero, but e.g., fabrication differences between devices can introduce a mismatch.
        Defaults to zero.
    time_offset : float (optional)
        The amount of time [ns] between the arrivals the two photons at the midpoint station (central frequency).
        Ideally this would be zero, but jitter in emission time and imperfect clock synchronization can make it nonzero.
        Defaults to zero.
    dispersion_coefficient : float (optional)
        Dispersion coefficient of the optical fiber [ps / nm / km] for the central frequency of the photon.
        Defaults to 17, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    dispersion_slope: float (optional)
        Dispersion slope of the optical fiber [ps / nm^2 / km] for the central frequency of the photon.
        Defaults to 0.058, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    wavelength : float (optional)
        Wavelength of photons [nm] (corresponding to average of the central frequencies of the photons).
        Defaults to 1550, which is telecom wavelength.

    Returns
    -------
    float
        Absolute value of correction to the visibility of two Gaussian photons due to dispersion.

    """
    v_without_dispersion = gaussian_photon_visibility_without_dispersion(time_sigma=time_sigma,
                                                                         time_offset=time_offset,
                                                                         frequency_mismatch=frequency_mismatch)
    v_with_dispersion = gaussian_photon_visibility(length_difference=length_difference,
                                                   time_sigma=time_sigma,
                                                   frequency_mismatch=frequency_mismatch,
                                                   time_offset=time_offset,
                                                   dispersion_coefficient=dispersion_coefficient,
                                                   dispersion_slope=dispersion_slope,
                                                   wavelength=wavelength)
    correction = v_without_dispersion - v_with_dispersion
    return correction


def gaussian_photon_visibility_without_dispersion(time_sigma, frequency_mismatch=0., time_offset=0.):
    """Visibility of two Gaussian photons accounting for frequency mismatch and time offset but not dispersion.

    Parameters
    ----------
    time_sigma : float
        Standard deviation [ns] of the photon's temporal probability density function, that is,
        |phi(t)|^2 is proportional to exp(- (1 / 2) (t / time_sigma)^2).
    frequency_mismatch : float (optional)
        Difference between central frequencies of the two Gaussian photons [GHz].
        Ideally this would be zero, but e.g., fabrication differences between devices can introduce a mismatch.
        Defaults to zero.
    time_offset : float (optional)
        The amount of time [ns] between the arrivals the two photons at the midpoint station (central frequency).
        Ideally this would be zero, but jitter in emission time and imperfect clock synchronization can make it nonzero.
        Defaults to zero.

    Returns
    -------
    float
        Visibility without accounting for dispersion.

    """
    return gaussian_photon_visibility(length_difference=0., time_sigma=time_sigma, time_offset=time_offset,
                                      frequency_mismatch=frequency_mismatch, dispersion_coefficient=0.,
                                      dispersion_slope=0.)


def gaussian_photon_visibility(length_difference, time_sigma, time_offset=0., frequency_mismatch=0.,
                               dispersion_coefficient=17, dispersion_slope=0.56, wavelength=1550):
    """Visibility between two Gaussian photons. Third-order dispersion is only included as first-order correction.

    This function accounts for first- and second-order corrections to the visibility (second-order corrections
    are needed to capture the effect of dispersion because there is no dispersion correction at first order).
    Therefore, this function returns an approximation.
    Only the effect of the frequency mismatch is accounted for exactly.

    Parameters
    ----------
    length_difference : float
        Difference in lengths [km] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    time_sigma : float
        Standard deviation [ns] of the photon's temporal probability density function, that is,
        |phi(t)|^2 is proportional to exp(- (1 / 2) (t / time_sigma)^2).
    frequency_mismatch : float (optional)
        Difference between central frequencies of the two Gaussian photons [GHz].
        Ideally this would be zero, but e.g., fabrication differences between devices can introduce a mismatch.
        Defaults to zero.
    time_offset : float (optional)
        The amount of time [ns] between the arrivals the two photons at the midpoint station (central frequency).
        Ideally this would be zero, but jitter in emission time and imperfect clock synchronization can make it nonzero.
        Defaults to zero.
    dispersion_coefficient : float (optional)
        Dispersion coefficient of the optical fiber [ps / nm / km] for the central frequency of the photon.
        Defaults to 17, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    dispersion_slope: float (optional)
        Dispersion slope of the optical fiber [ps / nm^2 / km] for the central frequency of the photon.
        Defaults to 0.058, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    wavelength : float (optional)
        Wavelength of photons [nm] (corresponding to average of the central frequencies of the photons).
        Defaults to 1550, which is telecom wavelength.

    Returns
    -------
    float
        Visibility.

    """
    beta_2 = group_velocity_dispersion_parameter(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient)
    # betas are already in SI units, now we still need to convert the other quantities
    length_difference_si_units = length_difference * 1E3  # km = 1E3 m
    time_sigma_si_units = time_sigma * 1E-9  # ns = 1E-9 s
    frequency_sigma = gaussian_frequency_sigma_from_time_sigma(time_sigma=time_sigma_si_units)  # also SI units
    frequency_mismatch_si_units = frequency_mismatch * 1E9  # GHz = 1E9 Hz
    time_offset_si_units = time_offset * 1E-9  # ns = 1E-9 s

    disp2 = (length_difference_si_units * beta_2 * frequency_sigma ** 2) ** 2
    vis = - 1 / 2 * (frequency_mismatch_si_units / frequency_sigma) ** 2
    vis -= (time_offset_si_units * frequency_sigma) ** 2 / (1 + disp2)
    vis = np.exp(vis)
    vis *= 1 / np.sqrt(1 + disp2)

    if dispersion_slope != 0:  # fixme shouldn't this be equality?
        return vis

    # if the dispersion slope / third-order dispersion is nonzero, we add a first-order correction
    beta_3 = third_order_dispersion(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient,
                                    dispersion_slope=dispersion_slope)
    first_order = (1 - disp2) ** 2
    first_order *= time_offset_si_units ** 2 * frequency_sigma ** 2
    first_order /= 3 * (1 + disp2)
    first_order += 1 - 3 * disp2
    first_order /= (1 + disp2) ** 2
    first_order *= length_difference_si_units * beta_3 * time_offset_si_units * frequency_sigma ** 4
    first_order = 1 - first_order
    vis *= first_order
    return vis


def gaussian_photon_relative_dispersion_correction(length_difference, time_sigma, time_offset=0.,
                                                   frequency_mismatch=0., dispersion_coefficient=17,
                                                   dispersion_slope=0.056, wavelength=1550):
    """Relative correction to visibility of Gaussian photons due to dispersion.

    Let the visibility be V = v_0 - v_1, where v_0 is the visibility in the absence of dispersion and v_1 is the
    correction due to dispersion.
    Then, this function returns v_1 / v_0, i.e., by what fraction the visibility must be adjusted due to dispersion.

    This function is based on a leading-order approximation of the visibility and hence returns an approximation.
    When the photon width is small, the approximation may not be accurate.

    Parameters
    ----------
    length_difference : float
        Difference in lengths [km] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    time_sigma : float
        Standard deviation [ns] of the photon's temporal probability density function, that is,
        |phi(t)|^2 is proportional to exp(- (1 / 2) (t / time_sigma)^2).
    frequency_mismatch : float (optional)
        Difference between central frequencies of the two Gaussian photons [GHz].
        Ideally this would be zero, but e.g., fabrication differences between devices can introduce a mismatch.
        Defaults to zero.
    time_offset : float (optional)
        The amount of time [ns] between the arrivals the two photons at the midpoint station (central frequency).
        Ideally this would be zero, but jitter in emission time and imperfect clock synchronization can make it nonzero.
        Defaults to zero.
    dispersion_coefficient : float (optional)
        Dispersion coefficient of the optical fiber [ps / nm / km] for the central frequency of the photon.
        Defaults to 17, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    dispersion_slope: float (optional)
        Dispersion slope of the optical fiber [ps / nm^2 / km] for the central frequency of the photon.
        Defaults to 0.058, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    wavelength : float (optional)
        Wavelength of photons [nm] (corresponding to average of the central frequencies of the photons).
        Defaults to 1550, which is telecom wavelength.

    Returns
    -------
    float
        Relative correction size due to dispersion, (V - V_no_dispersion) / V_no_dispersion.

    """
    v_without_dispersion = gaussian_photon_visibility_without_dispersion(time_sigma=time_sigma,
                                                                         time_offset=time_offset,
                                                                         frequency_mismatch=frequency_mismatch)
    v_with_dispersion = gaussian_photon_visibility(length_difference=length_difference,
                                                   time_sigma=time_sigma,
                                                   frequency_mismatch=frequency_mismatch,
                                                   time_offset=time_offset,
                                                   dispersion_coefficient=dispersion_coefficient,
                                                   dispersion_slope=dispersion_slope,
                                                   wavelength=wavelength)
    correction = v_without_dispersion - v_with_dispersion
    return correction / v_without_dispersion


def gaussian_visibility_kambs(length_difference, time_sigma, dispersion_coefficient=17, wavelength=1550):
    """Visibility for Gaussian photons as presented in the thesis of Benjamin Kambs (Section 7.2.1).

    The thesis can be found [here](https://publikationen.sulb.uni-saarland.de/handle/20.500.11880/28740).
    The expression does not account for time offset, frequency mismatch or third-order dispersion.

    Parameters
    ----------
    length_difference : float
        Difference in lengths [km] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    time_sigma : float
        Standard deviation [ns] of the photon's temporal probability density function, that is,
        |phi(t)|^2 is proportional to exp(- (1 / 2) (t / time_sigma)^2).
    dispersion_coefficient : float (optional)
        Dispersion coefficient of the optical fiber [ps / nm / km] for the central frequency of the photon.
        Defaults to 17, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    wavelength : float (optional)
        Wavelength of photons [nm] (corresponding to average of the central frequencies of the photons).
        Defaults to 1550, which is telecom wavelength.

    Returns
    -------
    float
        The visibility.

    """
    time_sigma_si_units = time_sigma * 1E-9  # ns = 1E-9 s
    frequency_sigma = gaussian_frequency_sigma_from_time_sigma(time_sigma_si_units)
    beta = 1 / (4 * frequency_sigma ** 2)  # note: not dispersion, something else Kambs defined

    beta_2 = group_velocity_dispersion_parameter(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient)
    length_difference_si_units = length_difference * 1E3  # km = 1E3 m
    diff_between_kappas = beta_2 * length_difference_si_units / 2

    kappa_1 = 0  # only difference should count
    kappa_2 = kappa_1 + diff_between_kappas

    gamma_1 = 1 + 16 * frequency_sigma ** 4 * kappa_1 ** 2
    gamma_2 = 1 + 16 * frequency_sigma ** 4 * kappa_2 ** 2
    a_1 = kappa_1 / beta ** 2 / gamma_1
    a_2 = kappa_2 / beta ** 2 / gamma_2
    delta_a = a_2 - a_1

    capital_gamma = 1 / (beta * gamma_1) + 1 / (beta * gamma_2)
    capital_gamma = 1 / capital_gamma

    capital_omega = 1 / capital_gamma + delta_a ** 2 * capital_gamma
    capital_omega = 4 / capital_omega
    capital_omega = np.sqrt(capital_omega)

    capital_sigma = beta * (gamma_1 + gamma_2)
    capital_sigma = np.sqrt(capital_sigma)

    vis = capital_omega / capital_sigma
    return vis


def gaussian_integrand(x, length_difference, frequency_sigma, time_offset, beta_2, beta_3):
    """Integrand that needs to be integrated over all x to obtain Gaussian visibility.

    All parameters should be passed in SI units.

    Parameters
    ----------
    x : float
        Parameter that is to be integrated over. It is a dimensionless quantity obtained by absorbing a one-over
        frequency_sigma in the frequency integral to make the integral dimensionless.
    length_difference : float
        Difference in lengths [m] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    frequency_sigma : float
        Standard deviation [s] of the photon's frequency probability density function, that is,
        |phi(omega)|^2 is proportional to exp(- (1 / 2) ((omega - omega_0) / frequency_sigma)^2).
    time_offset : float
        The amount of time [s] between the arrivals the two photons at the midpoint station (central frequency).
        Ideally this would be zero, but jitter in emission time and imperfect clock synchronization can make it nonzero.
    beta_2 : float
        Group-velocity-dispersion parameter.
    beta_3 : float
        Third-order-dispersion parameter.

    Returns
    -------
    float
        The value of the integrand for this value of x and the other parameters.

    """
    exponent = 1.j * time_offset * frequency_sigma * x
    exponent -= (1 - 1.j * length_difference * beta_2 * frequency_sigma ** 2) * x ** 2 / 2
    exponent += 1.j * length_difference * beta_3 * frequency_sigma ** 3 * x ** 3 / 6
    return np.exp(exponent)


def gaussian_photon_visibility_numerical(length_difference, time_sigma, time_offset=0., frequency_mismatch=0.,
                                         dispersion_coefficient=17, dispersion_slope=0.056, wavelength=1550,
                                         epsabs=1E-10, epsrel=1E-5):
    """Estimate of visibility between two Lorentzian photons based on numerical integration.

    Uses the function :func:`scipy.integrate.quad` to perform the numerical integration.

    Parameters
    ----------
    length_difference : float
        Difference in lengths [km] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    time_sigma : float
        Standard deviation [ns] of the photon's temporal probability density function, that is,
        |phi(t)|^2 is proportional to exp(- (1 / 2) (t / time_sigma)^2).
    frequency_mismatch : float (optional)
        Difference between central frequencies of the two Gaussian photons [GHz].
        Ideally this would be zero, but e.g., fabrication differences between devices can introduce a mismatch.
        Defaults to zero.
    time_offset : float (optional)
        The amount of time [ns] between the arrivals the two photons at the midpoint station (central frequency).
        Ideally this would be zero, but jitter in emission time and imperfect clock synchronization can make it nonzero.
        Defaults to zero.
    dispersion_coefficient : float (optional)
        Dispersion coefficient of the optical fiber [ps / nm / km] for the central frequency of the photon.
        Defaults to 17, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    dispersion_slope: float (optional)
        Dispersion slope of the optical fiber [ps / nm^2 / km] for the central frequency of the photon.
        Defaults to 0.058, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    wavelength : float (optional)
        Wavelength of photons [nm] (corresponding to average of the central frequencies of the photons).
        Defaults to 1550, which is telecom wavelength.
    epsabs : float (optional)
        Absolute error tolerance. Passed directly to :func:`scipy.integrate.quad`, see documentation of that function
        for more information. Default value is one that was found to balance accuracy and computational time
        for typical evaluations.
    epsrel : float (optional)
        Relative error tolerance. Passed directly to :func:`scipy.integrate.quad`, see documentation of that function
        for more information. Default value is one that was found to balance accuracy and computational time
        for typical evaluations.

    Returns
    -------
    float
        Estimate of the visibility.
    float
        Estimate of the error in the visibility estimate (see :func:`scipy.integrate.quad` for more info).

    """

    start_time = time.time()
    beta_2 = group_velocity_dispersion_parameter(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient)
    # betas are already in SI units, now we still need to convert the other quantities
    length_difference_si_units = length_difference * 1E3  # km = 1E3 m
    time_sigma_si_units = time_sigma * 1E-9  # ns = 1E-9 s
    frequency_sigma = gaussian_frequency_sigma_from_time_sigma(time_sigma=time_sigma_si_units)  # also SI units
    frequency_mismatch_si_units = frequency_mismatch * 1E9  # GHz = 1E9 Hz
    time_offset_si_units = time_offset * 1E-9  # ns = 1E-9 s
    beta_3 = third_order_dispersion(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient,
                                    dispersion_slope=dispersion_slope)

    function_to_integrate = partial(gaussian_integrand, length_difference=length_difference_si_units,
                                    frequency_sigma=frequency_sigma, time_offset=time_offset_si_units,
                                    beta_2=beta_2, beta_3=beta_3)
    mu, mu_err = scipy.integrate.quad(function_to_integrate, -np.inf, np.inf, limit=10000000,
                                      epsabs=epsabs, epsrel=epsrel, complex_func=True)
    vis = np.abs(mu) ** 2
    vis_err = abs(np.conj(mu) * mu_err + mu * np.conj(mu_err))
    factor = np.exp(- frequency_mismatch_si_units ** 2 / 2 / frequency_sigma ** 2) / 2 / np.pi
    vis *= factor
    vis_err *= factor
    duration = time.time() - start_time
    print(f"Numerically integrating Gaussian for time_sigma={time_sigma} took {duration} seconds.")
    print(1 - vis)
    return vis, vis_err


def lorentzian_photon_visibility(length_difference, tau, time_offset=0., frequency_mismatch=0.,
                                 dispersion_coefficient=17, dispersion_slope=0., wavelength=1550):
    """Visibility between two Lorentzian photons.

    This function evaluates an analytical expression for the visibility that does not account for nonzero time offset,
    frequency mismatch or third-order dispersion. A `NotImplementedError` will be raised when trying to evaluate
    outside this regime.

    Parameters
    ----------
    length_difference : float
        Difference in lengths [km] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    tau : float
        Radiative lifetime of the Lorentzian photons.
    frequency_mismatch : float (optional)
        Difference between central frequencies of the two Gaussian photons [GHz].
        Ideally this would be zero, but e.g., fabrication differences between devices can introduce a mismatch.
        Defaults to zero.
    time_offset : float (optional)
        The amount of time [ns] between the arrivals the two photons at the midpoint station (central frequency).
        Ideally this would be zero, but jitter in emission time and imperfect clock synchronization can make it nonzero.
        Defaults to zero.
    dispersion_coefficient : float (optional)
        Dispersion coefficient of the optical fiber [ps / nm / km] for the central frequency of the photon.
        Defaults to 17, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    dispersion_slope: float (optional)
        Dispersion slope of the optical fiber [ps / nm^2 / km] for the central frequency of the photon.
        Defaults to 0.058, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    wavelength : float (optional)
        Wavelength of photons [nm] (corresponding to average of the central frequencies of the photons).
        Defaults to 1550, which is telecom wavelength.

    Returns
    -------
    float
        The visibility.

    """
    if time_offset != 0. or frequency_mismatch != 0. or dispersion_slope != 0.:
        raise NotImplementedError("No analytical value for the visibility of Lorentzian photons is currently "
                                  "available in case the time offset, frequency mismatch or third-order dispersion "
                                  "is nonzero.")
    beta_2 = group_velocity_dispersion_parameter(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient)
    length_difference_si_units = length_difference * 1E3  # km = 1E3 m
    tau_si_units = tau * 1E-9  # ns = 1E-9 s
    exponential_coefficient = beta_2 * length_difference_si_units / tau_si_units ** 2 / 2
    fresnel_argument = cmath.sqrt(exponential_coefficient * 2 / np.pi)
    fresnel_s, fresnel_c = fresnel(fresnel_argument)
    mu = 1 - (1 - 1j) * fresnel_c - (1 + 1j) * fresnel_s
    visibility = mu * np.conj(mu)
    return visibility


def lorentzian_integrand(x, length_difference, tau, time_offset, frequency_mismatch, beta_2, beta_3):
    """Integrand that needs to be integrated over all x to obtain Lorentzian visibility.

    All parameters should be passed in SI units.

    Parameters
    ----------
    x : float
        Parameter that is to be integrated over. It is a dimensionless quantity obtained by absorbing a one-over
        frequency_sigma in the frequency integral to make the integral dimensionless.
    length_difference : float
        Difference in lengths [m] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    tau : float
        Radiative lifetime of the Lorentzian photons.
    time_offset : float
        The amount of time [s] between the arrivals the two photons at the midpoint station (central frequency).
        Ideally this would be zero, but jitter in emission time and imperfect clock synchronization can make it nonzero.
    frequency_mismatch : float
        Difference between central frequencies of the two photons [Hz].
        Ideally this would be zero, but e.g., fabrication differences between devices can introduce a mismatch.
    beta_2 : float
        Group-velocity-dispersion parameter.
    beta_3 : float
        Third-order-dispersion parameter.

    Returns
    -------
    float
        The value of the integrand for this value of x and the other parameters.

    """
    # all SI units
    exponent = 1 / 2 * 1.j * length_difference * beta_2 / tau ** 2 * x ** 2
    exponent += 1 / 6 * 1.j * length_difference * beta_3 / tau ** 3 * x ** 3
    if time_offset != 0.:
        exponent += 1.j * time_offset / tau * x
    div = ((1 - 2.j * tau * frequency_mismatch) ** 2 + x ** 2)
    return np.exp(exponent) / div / np.pi


def lorentzian_photon_visibility_numerical(length_difference, tau, time_offset=0., frequency_mismatch=0.,
                                           dispersion_coefficient=17, dispersion_slope=0.056, wavelength=1550,
                                           epsabs=1E-10, epsrel=1E-5):
    """Estimate of visibility between two Lorentzian photons based on numerical integration.

    Uses the function :func:`scipy.integrate.quad` to perform the numerical integration.

    Parameters
    ----------
    length_difference : float
        Difference in lengths [km] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    tau : float
        Radiative lifetime of the Lorentzian photons.
    frequency_mismatch : float (optional)
        Difference between central frequencies of the two Gaussian photons [GHz].
        Ideally this would be zero, but e.g., fabrication differences between devices can introduce a mismatch.
        Defaults to zero.
    time_offset : float (optional)
        The amount of time [ns] between the arrivals the two photons at the midpoint station (central frequency).
        Ideally this would be zero, but jitter in emission time and imperfect clock synchronization can make it nonzero.
        Defaults to zero.
    dispersion_coefficient : float (optional)
        Dispersion coefficient of the optical fiber [ps / nm / km] for the central frequency of the photon.
        Defaults to 17, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    dispersion_slope: float (optional)
        Dispersion slope of the optical fiber [ps / nm^2 / km] for the central frequency of the photon.
        Defaults to 0.058, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    wavelength : float (optional)
        Wavelength of photons [nm] (corresponding to average of the central frequencies of the photons).
        Defaults to 1550, which is telecom wavelength.
    epsabs : float (optional)
        Absolute error tolerance. Passed directly to :func:`scipy.integrate.quad`, see documentation of that function
        for more information. Default value is one that was found to balance accuracy and computational time
        for typical evaluations.
    epsrel : float (optional)
        Relative error tolerance. Passed directly to :func:`scipy.integrate.quad`, see documentation of that function
        for more information. Default value is one that was found to balance accuracy and computational time
        for typical evaluations.

    Returns
    -------
    float
        Estimate of the visibility.
    float
        Estimate of the error in the visibility estimate (see :func:`scipy.integrate.quad` for more info).

    """
    start_time = time.time()
    beta_2 = group_velocity_dispersion_parameter(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient)
    beta_3 = third_order_dispersion(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient,
                                    dispersion_slope=dispersion_slope)
    length_difference_si_units = length_difference * 1E3  # km = 1E3 m
    tau_si_units = tau * 1E-9  # ns = 1E-9 s
    frequency_mismatch_si_units = frequency_mismatch * 1E9  # GHz = 1E9 Hz
    time_offset_si_units = time_offset * 1E-9  # ns = 1E-9 s

    function_to_integrate = partial(lorentzian_integrand, length_difference=length_difference_si_units,
                                    tau=tau_si_units, time_offset=time_offset_si_units,
                                    frequency_mismatch=frequency_mismatch_si_units, beta_2=beta_2, beta_3=beta_3)
    mu, mu_err = scipy.integrate.quad(function_to_integrate, -np.inf, np.inf, limit=10000000,
                                      epsabs=epsabs, epsrel=epsrel, complex_func=True)
    vis = np.abs(mu) ** 2
    vis_err = abs(np.conj(mu) * mu_err + mu * np.conj(mu_err))
    duration = time.time() - start_time
    print(f"Numerically integrating Lorentzian for tau={tau} took {duration} seconds.")
    print(1 - vis)
    return vis, vis_err


def lorentzian_photon_visibility_dispersion_correction(length_difference, tau,
                                                       dispersion_coefficient=17, wavelength=1550):
    return 1 - lorentzian_photon_visibility(length_difference=length_difference, tau=tau,
                                            dispersion_coefficient=dispersion_coefficient, wavelength=wavelength)


def group_velocity_dispersion_parameter(wavelength, dispersion_coefficient):
    """Second derivative of the wave number beta, (d^2 beta(omega)) / (d omega^2).

    Beta is the wave number and omega the frequency; the function beta(omega) determines how waves behave in a medium.

    This function is based on Eq. 4.27 in the book Fiber Optics by Fedor Mitschke.

    Parameters
    ----------
    wavelength : float
        Wavelength [nm] at which the second derivative of the dispersion relation is evaluated.
    dispersion_coefficient : float
        Dispersion coefficient of the optical fiber [ps / nm / km] for the wavelength at which the second
        derivative of the dispersion relation is evaluated.

    Returns
    -------
    float
        Second derivative of the dispersion relation k(omega) with respect to omega [s^2 / m].

    """
    # first we convert everything to SI units for the calculation
    dispersion_coefficient_si_units = dispersion_coefficient * 1E-6  # ps = 1E-12 s, nm = 1E-9 m, km = 1E3 m
    wavelength_si_units = wavelength * 1E-9  # nm = 1E-9 m
    # as the output should be in SI units and speed of light is already in SI units, we can simply return the result
    return - wavelength_si_units ** 2 / (2 * np.pi * speed_of_light_in_vacuum) * dispersion_coefficient_si_units


def third_order_dispersion(wavelength, dispersion_coefficient, dispersion_slope):
    """Third derivative of the wave number beta, (d^3 beta(omega)) / (d omega^2).

    Beta is the wave number and omega the frequency; the function beta(omega) determines how waves behave in a medium.

    This function is based on Eq. 4.28 in the book Fiber Optics by Fedor Mitschke.

    Parameters
    ----------
    wavelength : float
        Wavelength [nm] at which the second derivative of the dispersion relation is evaluated.
    dispersion_coefficient : float
        Dispersion coefficient of the optical fiber [ps / nm / km] for the wavelength at which the second
        derivative of the dispersion relation is evaluated.

    Returns
    -------
    float
        Third derivative of the dispersion relation k(omega) with respect to omega [s^2 / m].

    """
    beta_2 = group_velocity_dispersion_parameter(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient)
    wavelength_si_units = wavelength * 1E-9  # nm = 1E-9 m
    dispersion_slope_si_units = dispersion_slope * 1E3  # ps = 1E-12 s, nm^2 = 1E-18 m^2, km = 1E3 m
    term_1 = wavelength_si_units ** 4 / (2 * np.pi * speed_of_light_in_vacuum) ** 2 * dispersion_slope_si_units
    term_2 = - wavelength_si_units / (np.pi * speed_of_light_in_vacuum) * beta_2
    beta_3 = term_1 + term_2
    return beta_3


def tau_from_std(std):
    r"""Obtain radiative lifetime from detection-time-distribution standard deviation of Lorentzian photon.

    The detection-time probability density function is $e^{- t / \tau} / sqrt(\tau) H(t)$,
    where $H(t)$ is the Heaviside step function.
    This distribution has $\tau$ as both the mean and standard deviation.

    Parameters
    ----------
    std : float
        Standard deviation.

    Returns
    -------
    float
        Radiative lifetime tau of the photon.

    """
    return std


def gaussian_time_sigma_from_fwhm(fwhm):
    """Obtain standard deviation for Gaussian photon from full width at half maximum (FWHM).

    Sigma is the standard deviation in real time, i.e.,
    |phi(t)|^2 is proportional to exp(- (1 / 2) (t / sigma)^2).
    This distribution is at full-width-half-maximum (FWHM) at |phi(FWHM/2)|^2 = 1 / 2,
    which is solved by FWHM = sigma 2 sqrt(2 ln 2).

    Parameters
    ----------
    fwhm : float
        Full width at half maximum of the Gaussian photon.

    Returns
    -------
    float
        Standard deviation of temporal probability distribution |phi(t)|^2.

    """
    return fwhm / (2 * np.sqrt(2 * np.log(2)))


def gaussian_time_sigma_from_frequency_sigma(frequency_sigma):
    """Get the standard deviation in frequency space from the standard deviation in the temporal domain.

    Parameters
    ----------
    frequency_sigma : float
        Standard deviation  of the photon's frequency probability density function, that is,
        |phi(omega)|^2 is proportional to exp(- (1 / 2) ((omega - omega_0) / frequency_sigma)^2).

    Returns
    -------
    float
        Standard deviation [(units of frequency_sigma)^-1] of the photon's temporal probability density function,
        that is, |phi(t)|^2 is proportional to exp(- (1 / 2) (t / time_sigma)^2).

    """
    return 1 / frequency_sigma / np.sqrt(2)


def gaussian_frequency_sigma_from_time_sigma(time_sigma):
    """Get the standard deviation in frequency space from the standard deviation in the temporal domain.

    Parameters
    ----------
    time_sigma : float
        Standard deviation of the photon's temporal probability density function, that is,
        |phi(t)|^2 is proportional to exp(- (1 / 2) (t / time_sigma)^2).

    Returns
    -------
    float
        Standard deviation [(units of time_sigma)^-1] of the photon's frequency probability density function, that is,
        |phi(omega)|^2 is proportional to exp(- (1 / 2) ((omega - omega_0) / frequency_sigma)^2).

    """
    return 1 / time_sigma / np.sqrt(2)


def gaussian_third_order_dispersion_dimensionless_parameter(length_difference, time_sigma, dispersion_coefficient=17,
                                                            dispersion_slope=0.056, wavelength=1550):
    """The dimensionless parameter beta_3 sigma^3 Delta L. This is the parameter in which the Gaussian visibility is
    expanded.

    The sigma in the parameter is the standard deviation in frequency space. It should not be confused with the
    standard deviation in time, which is the time_sigma parameter.
    They are related by frequency_sigma * time_sigma = 1 / sqrt(2).

    Parameters
    ----------
    length_difference : float
        Difference in lengths [km] between emitters and the midpoint station (i.e., L1 - L2 in the above figure).
        Only the absolute value of the length difference matters.
    time_sigma : float
        Standard deviation [ns] of the photon's temporal probability density function, that is,
        |phi(t)|^2 is proportional to exp(- (1 / 2) (t / time_sigma)^2).
    dispersion_coefficient : float (optional)
        Dispersion coefficient of the optical fiber [ps / nm / km] for the central frequency of the photon.
        Defaults to 17, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    dispersion_slope: float (optional)
        Dispersion slope of the optical fiber [ps / nm^2 / km] for the central frequency of the photon.
        Defaults to 0.058, which is a realistic value for single-mode fiber at telecom wavelength 1550 nm.
    wavelength : float (optional)
        Wavelength of photons [nm] (corresponding to average of the central frequencies of the photons).
        Defaults to 1550, which is telecom wavelength.

    """
    beta_3 = third_order_dispersion(wavelength=wavelength, dispersion_coefficient=dispersion_coefficient,
                                    dispersion_slope=dispersion_slope)
    length_difference_si_units = length_difference * 1E3  # km = 1E3 m
    time_sigma_si_units = time_sigma * 1E-9  # ns = 1E-9 s
    frequency_sigma = gaussian_time_sigma_from_frequency_sigma(time_sigma=time_sigma_si_units)
    return beta_3 * frequency_sigma ** 3 * length_difference_si_units
