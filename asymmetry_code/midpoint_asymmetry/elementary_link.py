from netsquid_magic.state_delivery_sampler import SingleClickDeliverySamplerFactory, \
    DoubleClickDeliverySamplerFactory, success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory
import numpy as np


def single_click_rate_and_fidelity(total_length, asymmetry_parameter, alpha_a=None, alpha_b=None, q=None,
                                   dark_count_prob=0., detector_efficiency=1., visibility=1., num_resolving=True,
                                   p_loss_length=.2, speed_of_light=2E5):
    """Calculate single-click rate and fidelity from analytical model in netsquid-magic."""
    success_prob, fid = single_click_success_prob_and_fidelity(total_length=total_length,
                                                               asymmetry_parameter=asymmetry_parameter,
                                                               alpha_a=alpha_a, alpha_b=alpha_b, q=q,
                                                               dark_count_prob=dark_count_prob,
                                                               detector_efficiency=detector_efficiency,
                                                               visibility=visibility, num_resolving=num_resolving,
                                                               p_loss_length=p_loss_length)
    length_a, length_b = link_distances(total_length=total_length, asymmetry_parameter=asymmetry_parameter)
    return success_prob_to_rate(length_a=length_a, length_b=length_b, success_prob=success_prob,
                                speed_of_light=speed_of_light), fid


def single_click_rate_and_fidelity_leading_order(total_length, asymmetry_parameter, q,
                                                 dark_count_prob=0., detector_efficiency=1., visibility=1.,
                                                 num_resolving=True, p_loss_length=.2, speed_of_light=2E5):
    """Calculate single-click rate and fidelity from analytical model to leading order."""
    success_prob, fid = single_click_success_prob_and_fidelity_leading_order(total_length=total_length, q=q,
                                                                             asymmetry_parameter=asymmetry_parameter,
                                                                             dark_count_prob=dark_count_prob,
                                                                             detector_efficiency=detector_efficiency,
                                                                             visibility=visibility,
                                                                             num_resolving=num_resolving,
                                                                             p_loss_length=p_loss_length)
    length_a, length_b = link_distances(total_length=total_length, asymmetry_parameter=asymmetry_parameter)
    return success_prob_to_rate(length_a=length_a, length_b=length_b, success_prob=success_prob,
                                speed_of_light=speed_of_light), fid


def single_click_success_prob_and_fidelity(total_length, asymmetry_parameter, alpha_a=None, alpha_b=None, q=None,
                                           dark_count_prob=0., detector_efficiency=1., visibility=1.,
                                           num_resolving=True, p_loss_length=.2):
    """Calculate single-click success probability and fidelity from analytical model in netsquid-magic."""
    length_a, length_b = link_distances(total_length=total_length, asymmetry_parameter=asymmetry_parameter)
    if q is not None:
        alpha_a, alpha_b = balanced_bright_state_params_from_q(q=q, length_a=length_a, length_b=length_b,
                                                               detector_efficiency=detector_efficiency)
    elif alpha_b is None:
        if alpha_a is None:
            raise ValueError("alpha_a, alpha_b and q cannot all be None.")
        alpha_b = balanced_bright_state_params(length_a=length_a, length_b=length_b, alpha_a=alpha_a,
                                               detector_efficiency=detector_efficiency)
    succ_prob, fid = \
        success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory(
            factory=SingleClickDeliverySamplerFactory,
            alpha_A=alpha_a, alpha_B=alpha_b,
            length_A=length_a, length_B=length_b,
            p_loss_length_A=p_loss_length,
            p_loss_length_B=p_loss_length,
            detector_efficiency=detector_efficiency, p_loss_init_A=0.,
            p_loss_init_B=0., num_resolving=num_resolving,
            dark_count_probability=dark_count_prob,
            p_fail_class_corr=0., coherent_phase=0.,
            visibility=visibility)
    return succ_prob, fid


def single_click_success_prob_calculated_directly(q, dark_count_prob=0., visibility=1., num_resolving=True):
    """Calculate single-click success probability and fidelity from analytical model in the paper."""
    r = 2 if num_resolving else 1
    p = 2 * q * (1 - q) * (1 - dark_count_prob) ** r
    p += 2 * dark_count_prob * (1 - dark_count_prob) * (1 - q) ** 2
    p += 1 / 2 * (2 - r) * q ** 2 * (1 - dark_count_prob) * (1 + visibility)
    return p


def single_click_success_prob_and_fidelity_leading_order(total_length, asymmetry_parameter, q, dark_count_prob=0.,
                                                         detector_efficiency=1., visibility=1., num_resolving=True,
                                                         p_loss_length=.2):
    """Calculate single-click success probability and fidelity from analytical model in paper to leading order."""
    r = 2 if num_resolving else 1
    p_tot = attenuation_efficiency(length=total_length, p_loss_length=p_loss_length) * detector_efficiency ** 2
    p_sum = 2 * np.sqrt(p_tot) * np.cosh(total_length / 2 * asymmetry_parameter * p_loss_length / 10 * np.log(10))

    s1 = 2 * (q + dark_count_prob)
    s2 = 1 / 2
    s2 += (1 - (2 + r) * dark_count_prob) / (q + dark_count_prob)
    s2 += (4 * r * q * dark_count_prob - (2 - r) * (1 + visibility) * q ** 2) / (8 * (q + dark_count_prob) ** 2)
    s2 *= q * (1 + np.sqrt(visibility)) / 2

    s3 = q / (q + dark_count_prob) * 3 / 2 - 1
    s3 *= q / p_tot * (1 + np.sqrt(visibility)) / 2

    succ_prob = s1
    fidelity = s2 - s3 * p_sum

    return succ_prob, fidelity


def double_click_rate_and_fidelity(total_length, asymmetry_parameter, dark_count_prob=0., detector_efficiency=1.,
                                   visibility=1., emission_fidelity=1., num_resolving=True,
                                   speed_of_light=2E+5, p_loss_length=.2):
    """Calculate double-click rate and fidelity from analytical model in netsquid-magic."""
    length_a, length_b = link_distances(total_length=total_length, asymmetry_parameter=asymmetry_parameter)
    success_prob, fid = double_click_success_prob_and_fidelity(total_length=total_length,
                                                               asymmetry_parameter=asymmetry_parameter,
                                                               dark_count_prob=dark_count_prob,
                                                               detector_efficiency=detector_efficiency,
                                                               visibility=visibility,
                                                               emission_fidelity=emission_fidelity,
                                                               num_resolving=num_resolving,
                                                               p_loss_length=p_loss_length)
    return success_prob_to_rate(length_a=length_a, length_b=length_b, success_prob=success_prob,
                                speed_of_light=speed_of_light), fid


def double_click_rate_and_fidelity_leading_order(total_length, asymmetry_parameter, dark_count_prob=0.,
                                                 detector_efficiency=1., visibility=1., emission_fidelity=1.,
                                                 num_resolving=True, speed_of_light=2E+5, p_loss_length=.2):
    """Calculate double-click rate and fidelity from analytical model in paper to leading order."""
    length_a, length_b = link_distances(total_length=total_length, asymmetry_parameter=asymmetry_parameter)
    success_prob, fid = double_click_success_prob_and_fidelity_leading_order(total_length=total_length,
                                                                             asymmetry_parameter=asymmetry_parameter,
                                                                             dark_count_prob=dark_count_prob,
                                                                             detector_efficiency=detector_efficiency,
                                                                             visibility=visibility,
                                                                             emission_fidelity=emission_fidelity,
                                                                             num_resolving=num_resolving,
                                                                             p_loss_length=p_loss_length)
    return success_prob_to_rate(length_a=length_a, length_b=length_b, success_prob=success_prob,
                                speed_of_light=speed_of_light), fid


def double_click_success_prob_and_fidelity(total_length, asymmetry_parameter, dark_count_prob=0.,
                                           detector_efficiency=1., visibility=1., emission_fidelity=1.,
                                           num_resolving=True, p_loss_length=0.2):
    """Calculate double-click success probability and fidelity from analytical model in netsquid-magic."""
    length_a, length_b = link_distances(total_length=total_length, asymmetry_parameter=asymmetry_parameter)
    succ_prob, fid = \
        success_prob_and_fidelity_from_heralded_state_delivery_sampler_factory(
            factory=DoubleClickDeliverySamplerFactory,
            length_A=length_a, length_B=length_b,
            emission_fidelity_A=emission_fidelity,
            emission_fidelity_B=emission_fidelity,
            p_loss_length_A=p_loss_length,
            p_loss_length_B=p_loss_length,
            detector_efficiency=detector_efficiency, p_loss_init_A=0.,
            p_loss_init_B=0., num_resolving=num_resolving,
            coin_prob_ph_ph=1., coin_prob_ph_dc=1.,
            coin_prob_dc_dc=1.,
            dark_count_probability=dark_count_prob,
            visibility=visibility,
            num_multiplexing_modes=1)
    return succ_prob, fid


def double_click_success_prob_and_fidelity_calculated_directly(total_length, asymmetry_parameter, dark_count_prob=0.,
                                                               detector_efficiency=1., visibility=1.,
                                                               emission_fidelity=1., num_resolving=True,
                                                               p_loss_length=0.2):
    """Calculate double-click success probability and fidelity from analytical model in paper."""
    r = 2 if num_resolving else 1
    p_tot = attenuation_efficiency(length=total_length, p_loss_length=p_loss_length) * detector_efficiency ** 2
    p_sum = 2 * np.sqrt(p_tot) * np.cosh(total_length / 2 * asymmetry_parameter * p_loss_length / 10 * np.log(10))
    qem = (4 * emission_fidelity - 1) ** 2 / 9

    d1_1 = p_tot / 2 * (1 - dark_count_prob) ** (2 * r)
    d1_2 = (2 - r) / 2 * (1 + visibility) - 4
    d1_2 *= dark_count_prob * p_tot * (1 - dark_count_prob) ** (r + 1)
    d1_3 = 4 * dark_count_prob ** 2 * (1 + p_tot) * (1 - dark_count_prob) ** 2
    d1 = d1_1 + d1_2 + d1_3

    d2 = 2 * dark_count_prob * (1 - dark_count_prob) ** (r + 1)
    d2 -= 4 * dark_count_prob ** 2 * (1 - dark_count_prob) ** 2

    d3_1 = qem / 4 * p_tot * (1 + visibility) * (1 - dark_count_prob) ** (2 * r)
    d3_2 = p_tot / 8 * (1 - dark_count_prob) ** (2 * r)
    d3_2 += (2 - r) / 8 * dark_count_prob * (1 - dark_count_prob) ** 2 * p_tot * (1 + visibility)
    d3_2 *= (1 - qem)
    d3_3 = - dark_count_prob * p_tot * (1 - dark_count_prob) ** (r + 1)
    d3_3 += dark_count_prob ** 2 * (1 + p_tot) * (1 - dark_count_prob) ** 2
    d3 = d3_1 + d3_2 + d3_3

    p_succ = d1 + d2 * p_sum
    fidelity = d3 + d2 * p_sum / 4
    fidelity /= d1 + d2 * p_sum

    return p_succ, fidelity


def double_click_rate_and_fidelity_calculated_directly(total_length, asymmetry_parameter, dark_count_prob=0.,
                                                       detector_efficiency=1., visibility=1., emission_fidelity=1.,
                                                       num_resolving=True, p_loss_length=0.2, speed_of_light=2.E5):
    """Calculate double-click rate and fidelity from analytical model in paper."""
    p_succ, fidelity = \
        double_click_success_prob_and_fidelity_calculated_directly(total_length=total_length,
                                                                   asymmetry_parameter=asymmetry_parameter,
                                                                   dark_count_prob=dark_count_prob,
                                                                   detector_efficiency=detector_efficiency,
                                                                   visibility=visibility,
                                                                   emission_fidelity=emission_fidelity,
                                                                   num_resolving=num_resolving,
                                                                   p_loss_length=p_loss_length)
    rate = p_succ * speed_of_light / (1 + asymmetry_parameter) / total_length
    return rate, fidelity


def double_click_success_prob_and_fidelity_leading_order(total_length, asymmetry_parameter, dark_count_prob=0.,
                                                         detector_efficiency=1., visibility=1., emission_fidelity=1.,
                                                         num_resolving=True, p_loss_length=0.2):
    """Calculate double-click success probability and fidelity from analytical model in paper to leading order."""
    r = 2 if num_resolving else 1
    p_tot = attenuation_efficiency(length=total_length, p_loss_length=p_loss_length) * detector_efficiency ** 2
    p_sum = 2 * np.sqrt(p_tot) * np.cosh(total_length / 2 * asymmetry_parameter * p_loss_length / 10 * np.log(10))
    qem = (4 * emission_fidelity - 1) ** 2 / 9

    # d1 = p_tot / 2 * (1 - 2 * r * dark_count_prob) + dark_count_prob * p_tot * ((2 - r) * (1 + visibility) / 2 - 4)
    d1 = p_tot / 2 - p_tot * dark_count_prob * (4 + r - (2 - r) * (1 + visibility) / 2)
    d2 = 2
    d3 = (qem * (1 + visibility) / 2 + (1 - qem) / 4) * (1 + 8 * dark_count_prob)
    d3 -= (2 - r) * (1 + visibility) ** 2 * qem * dark_count_prob / 2
    d4 = qem / p_tot * (2 * visibility + 1)

    p_succ = d1 + d2 * dark_count_prob * p_sum
    fidelity = d3 - d4 * dark_count_prob * p_sum

    return p_succ, fidelity


def link_distances(total_length, asymmetry_parameter, asymmetry_sign=+1):
    """Distances between Alice and midpoint and between Bob and midpoint."""
    return (1 - asymmetry_sign * asymmetry_parameter) * total_length / 2, \
           (1 + asymmetry_sign * asymmetry_parameter) * total_length / 2


def length_difference_from_asym_param(total_length, asymmetry_parameter, asymmetry_sign=+1):
    """Difference in lengths between Alice and midpoint and Bob and midpoint."""
    return asymmetry_parameter * asymmetry_sign * total_length


def asym_param_from_length_difference(total_length, length_difference):
    """Asymmetry parameter corresponding to a specific length difference."""
    asymmetry_sign = +1 if length_difference >= 0 else -1
    asymmetry_param = abs(length_difference) / total_length
    return asymmetry_param, asymmetry_sign


def success_prob_to_rate(success_prob, length_a, length_b, speed_of_light=2E+5):
    """Convert a success probability to a rate."""
    max_length = max([length_a, length_b])
    cycle_time = 2 * max_length / speed_of_light
    return success_prob / cycle_time


def balanced_bright_state_params(length_a, length_b, alpha_a, p_loss_length_a=0.2, p_loss_length_b=0.2,
                                 detector_efficiency=1., p_loss_init_a=0., p_loss_init_b=0.):
    """Calculate bright-state parameter used at Bob given the one at Alice such that they are balanced."""
    p_loss_init_a *= 1 - detector_efficiency
    p_loss_init_b *= 1 - detector_efficiency
    prob_a_detected = attenuation_efficiency(length=length_a, p_loss_length=p_loss_length_a) * (1 - p_loss_init_a)
    prob_b_detected = attenuation_efficiency(length=length_b, p_loss_length=p_loss_length_b) * (1 - p_loss_init_b)
    alpha_b = alpha_a * prob_a_detected
    alpha_b /= prob_b_detected
    return alpha_b


def balanced_bright_state_params_from_q(q, length_a, length_b, p_loss_length_a=0.2, p_loss_length_b=0.2,
                                        detector_efficiency=1., p_loss_init_a=0., p_loss_init_b=0.):
    """Bright-state parameter such that they are balanced in terms of the detection probability q defined in paper.

    q = p_A * alpha_A = p_B * alpha_B
    """
    prob_a_detected = attenuation_efficiency(length=length_a, p_loss_length=p_loss_length_a)
    prob_a_detected *= detector_efficiency
    prob_a_detected *= 1 - p_loss_init_a
    prob_b_detected = attenuation_efficiency(length=length_b, p_loss_length=p_loss_length_b)
    prob_b_detected *= detector_efficiency
    prob_b_detected *= 1 - p_loss_init_b
    alpha_a = q / prob_a_detected
    alpha_b = q / prob_b_detected
    return alpha_a, alpha_b


def attenuation_efficiency(length, p_loss_length=0.2):
    """Probability that a photon survives fiber attenuation."""
    return np.power(10, - length * p_loss_length / 10)
