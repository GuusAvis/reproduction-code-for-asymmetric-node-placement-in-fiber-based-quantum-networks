from netsquid_simulationtools.repchain_data_process import process_repchain_dataframe_holder, process_data_bb84, \
    process_data_duration, process_data
from argparse import ArgumentParser

from asymmetry_code.repeater_simulation.plot_qkd import plot_qkd_from_file


def plot_qkd_dataframe_holder(repchain_dataframe_holder):
    processed_data = process_repchain_dataframe_holder(repchain_dataframe_holder=repchain_dataframe_holder,
                                                       processing_functions=[process_data_bb84, process_data_duration])
    processed_data_file_name = "processed_data.csv"
    # [varied_param] = repchain_dataframe_holder.varied_parameters
    processed_data.to_csv(processed_data_file_name)
    plot_qkd_from_file(filename=processed_data_file_name)


def plot_data_in_directory(path, plot):
    process_data(raw_data_dir=path, process_bb84=True, process_teleportation=False, process_fidelity=False,
                 plot_processed_data=False)
    if plot:
        plot_qkd_from_file(filename="output.csv")


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument("-p", "--path", required=False, type=str, default="raw_data",
                        help="Path with raw data to plot.")
    parser.add_argument("--plot", dest="plot", action="store_true", help="Plot processed data.")
    args = parser.parse_args()
    plot_data_in_directory(path=args.path, plot=args.plot)
