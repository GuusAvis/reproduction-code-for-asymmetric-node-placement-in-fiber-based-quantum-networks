from netsquid_netconf.builder import Builder, ComponentBuilder, NetworkBuilder
import netsquid as ns
from copy import deepcopy
import numpy as np
import matplotlib.pyplot as plt
import yaml
from random import choices

from netsquid_netconf.linker import NetworkLinker
from netsquid_netconf.netconf import _get_objects


class ChainBuilder(Builder):
    """Builder used to generate repeater chain. Nodes can be placed asymmetrically.

    TODO update this docstring

    A chain is built with a total length of 'total_length' km and 'num_nodes' nodes.
    The nodes and connections of the chain are configured using :class:`netsquid_netconf.builders.ComponentBuilder`,
    which is given the parameters specified in 'node' to build the nodes (that are not midpoints) and the parameters
    specified in 'connection' to build the connections.
    If 'include_midpoints' is False, the connection class must take the argument `length`.
    If 'include_midpoints' is True, the connection class must take the arguments `length_A` and `length_B`.

    To generate asymmetric chains, specify a value for the fixed node-asymmetry parameter using 'asymmetry' as key.
    Additionally, the asymmetry signature of the chain must be defined under the key 'signs'.
    This can either be a list of `+1`s and `-1`s of length 'num_nodes', or one of the keywords
    'alternating', 'double_alternating', 'triple_alternating', 'min_mse_distance' or 'random'.

    To set the asymmetry parameters of the midpoints of the chain separately, the 'midpoint_asymmetry' key can be used
    (but only in case 'include_midpoints' is True).
    In that case, first all the non-midpoint nodes will be placed with asymmetry parameter 'asymmetry' and with a
    signature that is the signature of the entire chain, but with every second sign removed (these correspond
    to the midpoints). This creates a chain of ('num_nodes' - 1) / 2 + 1 nodes, with a fixed node-asymmetry parameter.
    After that, midpoints are placed in between each pair of neighbouring nodes such that
    their node-asymmetry parameter is 'midpoint_asymmetry' and their sign is as defined by the full asymmetry signature
    (i.e. the values that were not used in the first step are now used to place the midpoints).
    Note that the non-midpoint nodes of the resulting chain will now in general not have a node-asymmetry parameter
    equal to 'asymmetry' (this only holds if 'midpoint_asymmetry' is not defined).

    Raises
    ------
    ValueError
        If 'midpoint_asymmetry' is set and 'include_midpoints' is False.

    ValueError
        If 'num_nodes' is odd and 'include_midpoints' is True (only chains with odd number of nodes can have midpoints).

    """

    @classmethod
    def key(cls):
        return "chain"

    @classmethod
    def build(cls, config_dict):

        total_length = config_dict["total_length"]
        num_nodes = config_dict["num_nodes"]
        asymmetry = config_dict.get("asymmetry", 0)
        signs = config_dict.get("signs", "double_alternating")
        node_config_dict = config_dict["node"]
        classical_connection_config_dict = config_dict["classical_connection"]
        entanglement_connection_config_dict = config_dict["entanglement_connection"]
        draw = config_dict.get("draw", None)
        save_config = config_dict.get("save_config", None)
        spooled_fiber = config_dict.get("spooled_fiber", False)

        asymmetry = float(asymmetry) if asymmetry is not None else None

        node_coordinate_calculator = NodeCoordinateCalculator(
            total_length=total_length, num_nodes=num_nodes, asymmetry_parameter=asymmetry, spooled_fiber=spooled_fiber)

        if signs == "alternating":
            node_coordinate_calculator.set_signs_alternating()
        elif signs == "random":
            node_coordinate_calculator.set_signs_randomly()
        else:
            node_coordinate_calculator.signs = signs

        if draw is not None:
            ax = plt.axes()
            node_coordinate_calculator.draw(ax=ax)
            plt.savefig(f"{draw}.png")
            plt.clf()

        coordinates = node_coordinate_calculator.coordinates

        # set default node and connection
        if "node" not in ComponentBuilder.TYPES:
            ComponentBuilder.add_type(name="node", new_type=ns.nodes.Node)
        if "connection" not in ComponentBuilder.TYPES:
            ComponentBuilder.add_type(name="connection", new_type=ns.nodes.Connection)

        classical_connection_config_dict["connect_to"] = {"port_name_node1": "B",
                                                          "port_name_node2": "A",
                                                          "label": "classical_connection"}
        entanglement_connection_config_dict["connect_to"] = {"port_name_node1": "ENT_B",
                                                             "port_name_node2": "ENT_A",
                                                             "label": "entanglement_connection"}
        if "properties" not in node_config_dict:
            node_config_dict["properties"] = {}
        if "properties" not in classical_connection_config_dict:
            classical_connection_config_dict["properties"] = {}
        if "properties" not in entanglement_connection_config_dict:
            entanglement_connection_config_dict["properties"] = {}

        # build the component builder dictionary
        node_config_dict["properties"].update({"port_names": ["B"]})
        component_builder_config_dict = \
            cls._build(node_config_dict=node_config_dict,
                       classical_connection_config_dict=classical_connection_config_dict,
                       entanglement_connection_config_dict=entanglement_connection_config_dict,
                       coordinates=coordinates)

        new_config_dict = {"network": "chain",
                           "components": component_builder_config_dict}

        if save_config is not None:
            with open(f"{save_config}.yml", "w") as file:
                yaml.dump(new_config_dict, file, default_flow_style=False)

        return _get_objects(config=new_config_dict,
                            builders=[ComponentBuilder, NetworkBuilder],
                            linkers=[NetworkLinker])

    @staticmethod
    def _build(node_config_dict, classical_connection_config_dict, entanglement_connection_config_dict, coordinates):
        """Build the repeater chain."""

        node_config_dict["properties"].update({"end_node": True, "port_names": ["B", "ENT_B"], "num_positions": 1})
        component_builder_config_dict = {"node_0": deepcopy(node_config_dict)}
        node_config_dict["properties"].update({"end_node": False, "port_names": ["A", "ENT_A", "B", "ENT_B"],
                                               "num_positions": 2})
        for i in range(1, len(coordinates)):

            # node i
            if i == len(coordinates) - 1:  # the final node
                node_config_dict["properties"].update({"end_node": True, "port_names": ["A", "ENT_A"],
                                                       "num_positions": 1})
            component_builder_config_dict[f"node_{i}"] = deepcopy(node_config_dict)

            # classical connection between i - 1 and i
            classical_connection_config_dict["connect_to"].update({"node1": f"node_{i - 1}",
                                                                   "node2": f"node_{i}"})
            classical_connection_config_dict["properties"].update({"length": coordinates[i] - coordinates[i - 1]})
            component_builder_config_dict[f"cl_conn_{i - 1}"] = deepcopy(classical_connection_config_dict)

            # entanglement connection between i - 1 and i
            entanglement_connection_config_dict["connect_to"].update({"node1": f"node_{i - 1}",
                                                                      "node2": f"node_{i}"})
            entanglement_connection_config_dict["properties"].update({"length": coordinates[i] - coordinates[i - 1]})
            component_builder_config_dict[f"ent_conn_{i - 1}"] = deepcopy(entanglement_connection_config_dict)

        return component_builder_config_dict


class NodeCoordinateCalculator:
    """Used to calculate the coordinates of nodes in a chain defined by constant node-asymmetry parameter.

    The total chain length, number of nodes and the node-asymmetry parameters are fixed. Furthermore, the
    node-asymmetry parameter variance is zero. The direction of the asymmetry can be set.

    Parameters
    ----------
    total_length : float
        Total length of chain of nodes (arbitrary units).
    num_nodes : int
        Total number of nodes in the chain (including the end nodes, fixed at 0 and total_length).
    asymmetry_parameter : float
        Node asymmetry parameter of each node in the chain (difference between distances to neighbours divided
        by distance between those neighbours).
    spooled_fiber : bool (optional)
        If True, spooled fiber is used to increase the length of shorter segments in the chain to make it symmetric
        again. This results in a repeater chain that is effectively longer than specified by `total_length`.

    Notes
    -----

    Calculates the coordinates using the following equation:

    x_n = a_{n-1} 2 / (1 + sign_{n-1} A) x_{n-1} + b_{n-1} x_{n-2},
    where a_i = 2 / (1 + sign_{i} A) and b_i = - (1 - sign_{i} A) / (1 + sign{i} A),
    x_i is the coordinate of node i (with x_0 = 0 and x_{num_nodes} = total_length),
    A is the node-asymmetry parameter, and sign_i is the sign(direction) of th asymmetry of node i.

    First, x_1 is determined by starting with x_{num_nodes} = total_length and repeatedly applying the above
    equation until total_length = C x_1 + D x_0 = C x_1 is obtained (using x_0 = 0).
    This expansion to determine C is performed using a recursive function.
    We then set x_1 = total_length / C.
    Next, the equation can be used straightforwardly to calculate all x_i's sequentially.

    """

    def __init__(self, total_length, num_nodes, asymmetry_parameter, spooled_fiber=False):

        if asymmetry_parameter >= 1 or asymmetry_parameter < 0:
            raise ValueError("Asymmetry parameter must be 0 <= A < 1.")

        self.total_length = total_length
        self.num_nodes = num_nodes
        self._spooled_fiber = spooled_fiber
        self._signs = [None] * (self.num_nodes - 2)
        self._coordinates = [None] * num_nodes
        self._calculated = False
        self.asymmetry_parameter = asymmetry_parameter

    @property
    def asymmetry_parameter(self):
        return self._asymmetry_parameter

    @asymmetry_parameter.setter
    def asymmetry_parameter(self, asymmetry_parameter):
        self._asymmetry_parameter = asymmetry_parameter
        self._calculated = False

    @property
    def spooled_fiber(self):
        return self._spooled_fiber

    @spooled_fiber.setter
    def spooled_fiber(self, spooled_fiber):
        self._spooled_fiber = spooled_fiber
        self._calculated = False

    @property
    def signs(self):
        """Signs indicating the direction of each asymmetry (+1: left side is larger, -1: right side is larger).

        Returns
        -------
        signs : list of +1 and -1
            signs[i] indicates asymmetry direction of node[i + 1], where node[0] is the end node at coordinate zero.

        """
        return self._signs

    @signs.setter
    def signs(self, signs):
        """Signs indicating the direction of each asymmetry (+1: left side is larger, -1: right side is larger).

        Parameters
        ----------
        signs : list of +1 and -1
            signs[i] indicates asymmetry direction of node[i + 1], where node[0] is the end node at coordinate zero.

        """
        if len(signs) != self.num_nodes - 2:
            raise ValueError(f"Signs must have length {self.num_nodes - 2}" f", not {len(signs)}.")
        self._calculated = False
        self._signs = signs

    def set_signs_randomly(self):
        """Choose directionality of all asymmetries randomly."""
        self._calculated = False
        self._signs = choices(population=[+1, -1], k=self.num_nodes - 2)

    def set_signs_alternating(self):
        """Choose directionality of all asymmetries such that it is alternating.

        This means that if node[i] if further away from node[i - 1] than from node[i + 1],
        than node[i + 1] will be further way from node[i + 2] than from node [i].
        The sequence is started such that signs[0] = +1, i.e. the first segment will be larger than the second.

        """
        self._calculated = False
        self._signs = [+1 if i % 2 == 0 else -1 for i in range(self.num_nodes - 2)]

    @property
    def coordinates(self):
        """Coordinates of nodes in a chain characterized by the parameters given at initialization and the set signs."""
        if not self._calculated:
            self._calculate_coordinates()
        return self._coordinates

    def draw(self, ax, ypos=0, mark_odd_nodes=False):
        """Draw a chain of nodes with coordinates given by `self.coordinates`.

        Parameters
        ----------
        ax : :class:`matplotlib.axes.Axes`
            Axis to draw chain in.
        ypos : float
            Height in axis at which chain is drawn.
        mark_odd_nodes : bool
            Set True if odd nodes in the chain should use a different marker than even nodes.
            If the odd nodes represent midpoint stations for heralded entanglement generation this can be helpful.

        """
        midpoint_marker = "X" if mark_odd_nodes else "o"
        coordinates = self.coordinates
        # plt.xlim(0, coordinates[-1])
        # plt.ylim(0.5, 1.5)
        y = np.ones(np.shape(coordinates)) * ypos
        rep_coords = [coordinates[i] for i in range(0, len(coordinates), 2)]
        p = ax.plot(rep_coords, y[:len(rep_coords)], "o", markersize=10, color="black")
        mid_coords = [coordinates[i] for i in range(1, len(coordinates), 2)]
        ax.plot(mid_coords, y[:len(mid_coords)], midpoint_marker, markersize=10, color=p[0].get_color())
        ax.hlines(ypos, 0, coordinates[-1], color=p[0].get_color())
        ax.axis("off")

    def _recursive_function(self, x, n, signs, asymmetry_parameter):
        """Recursive function used to determine the coordinates of the second node in the chain.

        Parameters
        ----------
        x : float
            Value the recursive function acts upon.
        n : int
            Node of which the coordinate is expanded into coordinates of earlier nodes in the chain.
        signs : list of int
            Signs to use.
        asymmetry_parameter : float
            Asymmetry of nodes in the chain.

        """
        if n == 0:
            return x
        sign = signs[n - 1]  # sign of asymmetry of node n - 1
        y = sign * asymmetry_parameter
        A = 2 / (1 + y)
        B = - (1 - y) / (1 + y)
        if n == 1:
            return A * x
        return (self._recursive_function(A * x, n - 1, signs, asymmetry_parameter) +
                self._recursive_function(B * x, n - 2, signs, asymmetry_parameter))

    def _asym_coords(self, total_length, asymmetry, num_nodes, signs):
        # Calculate coordinates based on asymmetry
        coords = [0] * num_nodes
        # Calculate second repeater coord recursively
        coords[1] = total_length / self._recursive_function(
            1, num_nodes - 2, signs, asymmetry)
        # Calculate the rest iteratively
        Apos = 2 / (1 + asymmetry)
        Aneg = 2 / (1 - asymmetry)
        Bpos = - (1 - asymmetry) / (1 + asymmetry)
        Bneg = - (1 + asymmetry) / (1 - asymmetry)
        for n in range(2, num_nodes - 1):
            if signs[n - 2] == +1:  # sign of asymmetry of node n - 1
                coords[n] = Apos * coords[n - 1] + Bpos * coords[n - 2]
            else:
                coords[n] = Aneg * coords[n - 1] + Bneg * coords[n - 2]
        coords[num_nodes - 1] = total_length
        if not np.isclose(coords[num_nodes - 1], total_length):
            print(f"WARNING: {coords[num_nodes - 1]} != {total_length}")
        return coords

    def _calculate_coordinates(self):
        """Perform calculation of node coordinates for parameters specified at initialization and the set signs.

        Raises
        ------
        RuntimeError : if self.signs is not set before this method is called.

        """

        if None in self._signs:
            raise RuntimeError("Signs are not set yet.")
        coordinates_without_spooled_fiber = self._asym_coords(total_length=self.total_length, num_nodes=self.num_nodes,
                                                              signs=self._signs, asymmetry=self._asymmetry_parameter)
        if not self.spooled_fiber:
            self._coordinates = coordinates_without_spooled_fiber
        else:
            distances_between_nodes = [coordinates_without_spooled_fiber[i + 1] - coordinates_without_spooled_fiber[i]
                                       for i in range(self.num_nodes - 1)]
            largest_distance = max(distances_between_nodes)
            self._coordinates = [largest_distance * i for i in range(self.num_nodes)]
        self._calculated = True
