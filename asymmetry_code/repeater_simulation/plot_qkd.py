import matplotlib.pyplot as plt
import pandas as pd
from argparse import ArgumentParser


plot_args_1 = {"color": "limegreen",
               "linestyle": "-",
               "alpha": 0.6,
               "marker": "D",
               "markersize": 5,
               "markerfacecolor": "darkgreen",
               "markeredgecolor": "darkgreen",
               "linewidth": 2,
               }


plot_args_2 = {"color": "gold",
               "linestyle": "-",
               "alpha": 1.,
               "marker": "o",
               "markersize": 5,
               "markerfacecolor": "orange",
               "markeredgecolor": "orange",
               "linewidth": 2,
               }


def set_rc():
    """Formatting matplotlib's rc."""
    plt.rcParams.update({
        # "text.usetex": True,
        "font.size": 14,
    })
    # plt.ticklabel_format(style="sci", scilimits=(-2, 2))
    # plt.margins(x=0)


def plot_secret_key_rate(data, ax, label, **plot_args):
    """Plot secret-key rate.

    Parameters
    ----------
    data : :class:`pandas.DataFrame`
        Processed data for which to plot secret-key rate.
    ax : :class:`matplotlib.axes.Axes`
        Axis to create plot in.
    label : str or None
        Label include with the plotted data. Can be used to create a legend. None by default.
    plot_args
        Arguments to pass on to the plotting script.

    """
    ax.errorbar(x=data.asymmetry, y=data.sk_rate, yerr=data.sk_error, label=label, **plot_args)
    ax.set_ylabel("secret key rate [Hz]")
    ax.set_xlabel(r"$\mathcal{A}_{\mathrm{chain}}$", fontsize=20)


def plot_qber(data, ax, label, **plot_args):
    """Plot Quantum Bit Error Rate (QBER).

    Only plots X QBER; it is assumed the QBER is the same in all Pauli bases.

    Parameters
    ----------
    data : :class:`pandas.DataFrame`
        Processed data for which to plot QBER.
    ax : :class:`matplotlib.axes.Axes`
        Axis to create plot in.
    label : str or None
        Label include with the plotted data. Can be used to create a legend. None by default.
    plot_args
        Arguments to pass on to the plotting script.

    """
    ax.errorbar(x=data.asymmetry, y=data.Qber_x, yerr=data.Qber_x_error, label=label, **plot_args)
    ax.set_ylabel("QBER")
    ax.set_xlabel(r"$\mathcal{A}_{\mathrm{chain}}$", fontsize=20)


def plot_duration(data, ax, label, **plot_args):
    """Plot generation duration.

    Parameters
    ----------
    data : :class:`pandas.DataFrame`
        Processed data for which to plot duration results.
    ax : :class:`matplotlib.axes.Axes`
        Axis to create plot in.
    label : str or None
        Label include with the plotted data. Can be used to create a legend. None by default.
    plot_args
        Arguments to pass on to the plotting script.

    """
    ax.errorbar(x=data.asymmetry, y=data.duration_per_success, yerr=data.duration_per_success_error, label=label,
                **plot_args)
    ax.set_ylabel("generation duration [s]")
    ax.set_xlabel(r"$\mathcal{A}_{\mathrm{chain}}$", fontsize=20)


def plot_qkd(data, axs, label=None, **plot_args):
    """Plot secret-key rate, QBER and generation duration.

    Parameters
    ----------
    data : :class:`pandas.DataFrame`
        Processed data for which to plot QKD results.
    axs : array of three :class:`matplotlib.axes.Axes`
        Axes to create plots in.
    label : str or None
        Label include with the plotted data. Can be used to create a legend. None by default.
    plot_args
        Arguments to pass on to the plotting script.

    """

    plot_secret_key_rate(data=data, ax=axs[0], label=label, **plot_args)
    plot_qber(data=data, ax=axs[1], label=label, **plot_args)
    plot_duration(data=data, ax=axs[2], label=label, **plot_args)

    return axs


def plot_qkd_from_file(filename, label=None, only_skr=False):
    """Plot QKD data of two data sets together by loading data from CSV files.

    Parameters
    ----------
    filename : str
        Name of CSV file holding data set.
    label : str
        Label for data set.
    only_skr : bool
        Set True if only secret-key rate should be shown (not qber and generation duration). False by default.

    Returns
    -------
    fig : :class:`matplotlib.figure.Figure`
        The figure.
    axs : array of :class:`matplotlib.axes.Axes`
        The axes (subplots) of the figure.

    """
    set_rc()
    data = pd.read_csv(filename)
    if only_skr:
        fig, ax = plt.subplots(1, 1)
        plot_secret_key_rate(data=data, label=label, ax=ax, **plot_args_1)
        axs = [ax]
    else:
        fig, axs = plt.subplots(1, 3)
        plot_qkd(data=data, label=label, axs=axs, **plot_args_1)
    return fig, axs


def plot_qkd_comparison_from_files(filename1, filename2, label1=None, label2=None, only_skr=False):
    """Plot QKD data of two data sets together by loading data from CSV files.

    Parameters
    ----------
    filename1 : str
        Name of CSV file holding first data set.
    filename2 : str
        Name of CSV file holding second data set.
    label1 : str or None (optional)
        Label for first data set.
        If None, no label is added.
    label2 : str or None (optional)
        Label for second data set.
        If None, no label is added.
    only_skr : bool
        Set True if only secret-key rate should be shown (not qber and generation duration). False by default.

    Returns
    -------
    fig : :class:`matplotlib.figure.Figure`
        The figure.
    axs : array of :class:`matplotlib.axes.Axes`
        The axes (subplots) of the figure.

    """
    set_rc()
    data1 = pd.read_csv(filename1)
    data2 = pd.read_csv(filename2)
    if only_skr:
        fig, ax = plt.subplots(1, 1)
        plot_secret_key_rate(data=data1, label=label1, ax=ax, **plot_args_1)
        plot_secret_key_rate(data=data2, label=label2, ax=ax, **plot_args_2)
        axs = [ax]
    else:
        fig, axs = plt.subplots(3, 1, figsize=[5, 10], sharex=True)
        plot_qkd(data=data1, label=label1, axs=axs, **plot_args_1)
        plot_qkd(data=data2, label=label2, axs=axs, **plot_args_2)
    if label1 is not None or label2 is not None:
        axs[0].legend()
    for ax in axs:
        ax.set_ylim([0, None])
    plt.tight_layout()
    return fig, axs


def save_plot(path):
    # fig = plt.gcf()
    # fig.set_size_inches(10, 8)
    if path[-4:] != ".png":
        path += ".png"
    plt.savefig(path, bbox_inches="tight", dpi=300)


if __name__ == "__main__":

    parser = ArgumentParser()
    parser.add_argument('-f', '--filename', type=str, required=False, default="output.csv",
                        help="Filename of the (first) processed data set.")
    parser.add_argument('-c', '--compare', type=str, required=False, default=None,
                        help="Filename of the second processed data set.")
    parser.add_argument('-l', '--label', type=str, required=False, default=None,
                        help="Label used for (first) data set.")
    parser.add_argument('-l2', '--label2', type=str, required=False, default=None,
                        help="Label used for second data set.")
    parser.add_argument('-s', '--secret_key_rate', dest="only_skr", action="store_true",
                        help="Show only secret key rate (not qber and generation duration).")
    args = parser.parse_args()
    if args.compare is None:
        plot_qkd_from_file(filename=args.filename, label=args.label)
    else:
        plot_qkd_comparison_from_files(filename1=args.filename, filename2=args.compare,
                                       label1=args.label, label2=args.label2, only_skr=args.only_skr)
    plt.show()
