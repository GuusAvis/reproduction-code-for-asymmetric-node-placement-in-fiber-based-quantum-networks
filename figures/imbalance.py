import matplotlib.pyplot as plt
import math
import numpy as np
from asymmetry_code.midpoint_asymmetry.elementary_link import single_click_success_prob_and_fidelity_leading_order, \
    double_click_success_prob_and_fidelity_leading_order, asym_param_from_length_difference


if __name__ == "__main__":
    q = 4E-3
    pdc = 3E-4
    length = 100
    length_differences = np.linspace(0., 100.)

    plt.rcParams.update({'font.size': 13})
    fig, axs = plt.subplots(2, 1, figsize=(4, 6), sharex=True)
    asyms = [asym_param_from_length_difference(length_difference=l, total_length=length)[0]
             for l in length_differences]
    ps_and_fs_single_click = [single_click_success_prob_and_fidelity_leading_order(total_length=length,
                                                                                   asymmetry_parameter=asym,
                                                                                   q=q, dark_count_prob=pdc)
                              for asym in asyms]
    ps_and_fs_double_click = [double_click_success_prob_and_fidelity_leading_order(total_length=length,
                                                                                   asymmetry_parameter=asym,
                                                                                   dark_count_prob=pdc)
                                for asym in asyms]
    ps_single_click = [x[0] for x in ps_and_fs_single_click]
    fs_single_click = [x[1] for x in ps_and_fs_single_click]
    ps_double_click = [x[0] for x in ps_and_fs_double_click]
    fs_double_click = [x[1] for x in ps_and_fs_double_click]

    axs[0].plot(length_differences, ps_single_click, label="single click", linestyle="--", color="black")
    axs[0].plot(length_differences, ps_double_click, label="double click", color="black")
    axs[1].plot(length_differences, fs_single_click, label="single click", linestyle="--", color="black")
    axs[1].plot(length_differences, fs_double_click, label="double click", color="black")

    axs[0].set(ylabel="success probability", xlabel=r"$\Delta L$ [km]")
    axs[1].set(ylabel="fidelity", xlabel=r"$\Delta L$ [km]")

    axs[0].set_ylim([0., 0.01])
    axs[0].set_xlim(0., length_differences[-1])
    # axs[0].set_xticks([10 * x for x in range(math.floor((length_differences[-1]) / 10)])
    axs[0].set_xticks([20 * x for x in range(math.floor(length_differences[-1] / 20) + 1)])
    axs[1].set_ylim([None, 1.])
    axs[0].grid()
    axs[1].grid()

    plt.legend()
    plt.tight_layout()
    plt.savefig("figures/imbalance", dpi=480)

