import matplotlib.pyplot as plt
import numpy as np
from asymmetry_code.midpoint_asymmetry.dispersion import lorentzian_photon_visibility_numerical, \
    lorentzian_photon_visibility_dispersion_correction, gaussian_photon_visibility_numerical, \
    gaussian_photon_visibility_dispersion_correction


if __name__ == "__main__":
    length_difference = 40
    start = -2
    stop = 1
    num_numerical = 30
    ymin = 1E-4
    epsabs = 1E-6
    epsrel = 1E-10
    plt.rcParams.update({'font.size': 13})
    widths_ana = np.logspace(start, stop, 1000)
    widths_numer = np.logspace(start, stop, num_numerical)
    widths_numer_lor = [width for width in widths_numer
                        if (lorentzian_photon_visibility_dispersion_correction(length_difference=length_difference,
                                                                               tau=width) > ymin)]
    numers_lor = [lorentzian_photon_visibility_numerical(length_difference=length_difference, tau=width,
                                                         epsabs=epsabs, epsrel=epsrel)
                  for width in widths_numer_lor]
    numer_vis_lor = [1 - x[0] for x in numers_lor]
    numer_err_lor = [x[1] for x in numers_lor]
    anas_lor = [lorentzian_photon_visibility_dispersion_correction(length_difference=length_difference, tau=width)
                for width in widths_ana]
    widths_numer_gau = [width for width in widths_numer
                        if (gaussian_photon_visibility_dispersion_correction(length_difference=length_difference,
                                                                             time_sigma=width) > ymin)]
    numers_gau = [gaussian_photon_visibility_numerical(length_difference=length_difference, time_sigma=width,
                                                       epsabs=epsabs, epsrel=epsrel)
                  for width in widths_numer_gau]
    numer_vis_gau = [1 - x[0] for x in numers_gau]
    numer_err_gau = [x[1] for x in numers_gau]
    anas_gau = [gaussian_photon_visibility_dispersion_correction(length_difference=length_difference, time_sigma=width)
                for width in widths_ana]
    # diffs_lor = [abs(lorentzian_photon_visibility_dispersion_correction(length_difference=length_difference,
    #                                                                     tau=width)
    #                  - numerical_result) / numerical_result
    #              for width, numerical_result in zip(widths_numer_lor, numer_vis_lor)]
    # diffs_gau = [abs(gaussian_photon_visibility_dispersion_correction(length_difference=length_difference,
    #                                                                   time_sigma=width)
    #                  - numerical_result) / numerical_result
    #              for width, numerical_result in zip(widths_numer_gau, numer_vis_gau)]
    # plt.plot(widths_numer_lor, diffs_lor, linestyle="--", label="Lorentzian")
    # plt.plot(widths_numer_gau, diffs_gau, linestyle=":", label="Gaussian")
    plt.errorbar(widths_numer_lor, numer_vis_lor, yerr=numer_err_lor, fmt="o", markersize=4,
                 color="black")
    plt.errorbar(widths_numer_gau, numer_vis_gau, yerr=numer_err_gau, fmt="o", markersize=4,
                 color="black")
    plt.plot(widths_ana, anas_lor, linestyle="-", label="Lorentzian", color="black", linewidth=1.5)
    plt.plot(widths_ana, anas_gau, linestyle="--", label="Gaussian", color="black", linewidth=1.5)
    plt.xlabel("temporal photon length [ns]")
    plt.ylabel(r"1 - $V$")
    plt.ylim(ymin, 1)
    plt.xlim(widths_ana[0], widths_ana[-1])
    plt.loglog()
    plt.legend()
    plt.tight_layout()
    plt.savefig("figures/compare_analytical_numerical_visibility", dpi=480)
