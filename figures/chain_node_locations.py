from asymmetry_code.repeater_simulation.chain_builder import NodeCoordinateCalculator
import matplotlib.pyplot as plt


calculator = NodeCoordinateCalculator(total_length=1000, num_nodes=9, asymmetry_parameter=0., spooled_fiber=False)
calculator.set_signs_alternating()

fig, ax = plt.subplots(1, 1, figsize=(15, 5))

for i, asym in enumerate([0., 0.25, 0.5]):
    calculator.asymmetry_parameter = asym
    ax.text(0, -i - .3 + 0.2, r"$\mathcal{A}_{\mathrm{chain}} = $ " + str(asym), fontsize=40)
    calculator.draw(ax, ypos=-i - .3)

plt.tight_layout()
plt.savefig("figures/chain_node_locations", dpi=480)
