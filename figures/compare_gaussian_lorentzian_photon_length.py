import matplotlib.pyplot as plt
import numpy as np
from asymmetry_code.midpoint_asymmetry.dispersion import gaussian_photon_visibility_dispersion_correction, \
    lorentzian_photon_visibility_dispersion_correction, tau_from_std

if __name__ == "__main__":
    plt.clf()
    plt.rcParams.update({'font.size': 13})
    widths = np.logspace(-2, 4, 1000)
    linestyles = ["-", ":"]
    length = 40
    gaussian_visibility = [gaussian_photon_visibility_dispersion_correction(
        length_difference=length,
        time_sigma=width)
                           for width in widths]
    lorentzian_visibility = [lorentzian_photon_visibility_dispersion_correction(length_difference=length,
                                                                                tau=tau_from_std(width))
                             for width in widths]
    plotargs = dict(color="black", linewidth=2)
    plt.plot(widths, lorentzian_visibility, label=fr"Lorentzian", linestyle="-", **plotargs)
    plt.plot(widths, gaussian_visibility, label=fr"Gaussian", linestyle="--", **plotargs)
    # plt.plot(widths, [1E-3] * len(widths), label="1E-3", linestyle=":")

    plt.ylim(1E-7, 1)
    plt.xlim(widths[0], widths[-1])
    plt.xlabel("temporal photon length [ns]")
    plt.ylabel(r"1 - $V$")
    arrowprops = dict(facecolor='black', width=1, frac=.0001, headwidth=6)
    markerstyle = dict(marker="o", markerfacecolor="grey", markeredgecolor="grey", markersize=6)

    vline_args = dict(ymin=0., ymax=1., color="grey", alpha=.8, linestyle="-", linewidth=1)
    annotate_args = dict(fontsize=13, annotation_clip=False, ha="center")
    annotate_y = 1.3

    # some QDs (Rota)
    plt.vlines(x=2.5E-2, **vline_args)
    plt.annotate("QD1", xy=(2.5E-2, annotate_y), **annotate_args)

    # some other QDs (He)
    plt.vlines(x=416E-3, **vline_args)
    plt.annotate("QD2", xy=(416E-3, annotate_y), **annotate_args)

    # yet more QDs (Schauber)
    plt.vlines(x=2., **vline_args)
    plt.annotate("QD3", xy=(2., annotate_y), **annotate_args)

    # NV, some types of ions
    plt.vlines(x=12., **vline_args)
    plt.annotate("NV", xy=(11., annotate_y), **annotate_args)

    # multiplexed repeater systems
    plt.vlines(x=26., **vline_args)
    plt.annotate("SPDC", xy=(33, annotate_y), **annotate_args)

    # cSPDC
    # plt.vlines(400, **vline_args)

    # Ca ions
    # plt.annotate("5", xy=(1.75E3, 3E-5))
    plt.vlines(2E3, **vline_args)
    plt.annotate(r"$\mathrm{Ca}^+$", xy=(2E3, annotate_y), **annotate_args)

    # plt.grid()
    plt.loglog()
    plt.legend()
    plt.tight_layout()
    plt.savefig("figures/compare_gaussian_lorentzian_photon_length", dpi=480)
