from asymmetry_code.repeater_simulation.plot_qkd import plot_qkd_comparison_from_files, save_plot


if __name__ == "__main__":
    fig, axs = plot_qkd_comparison_from_files(filename1="simulation/no_spooled_fiber/output.csv",
                                              filename2="simulation/spooled_fiber/output.csv",
                                              label1="asymmetric", label2="extended fiber")
    axs[0].set_xlim([0., .5])
    axs[0].set_xticks([0.1 * x for x in range(6)])

    save_plot("figures/repeater_chain")
