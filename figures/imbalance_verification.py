import matplotlib.pyplot as plt
import numpy as np
from asymmetry_code.midpoint_asymmetry.elementary_link import single_click_rate_and_fidelity, \
    single_click_rate_and_fidelity_leading_order, double_click_rate_and_fidelity, \
    double_click_rate_and_fidelity_leading_order, asym_param_from_length_difference

if __name__ == "__main__":
    q = 4E-3
    pdc = 1E-4
    length = 100
    length_differences = np.linspace(0., 50.)

    plt.rcParams.update({'font.size': 13})
    fig, axs = plt.subplots(1, 2, figsize=(10, 4))
    asyms = [asym_param_from_length_difference(length_difference=l, total_length=length)[0]
             for l in length_differences]
    rates_and_fids_single_click_exact = [single_click_rate_and_fidelity(total_length=length,
                                                                        asymmetry_parameter=asym,
                                                                        q=q, dark_count_prob=pdc)
                                         for asym in asyms]
    rates_single_click_exact = [x[0] for x in rates_and_fids_single_click_exact]
    fids_single_click_exact = [x[1] for x in rates_and_fids_single_click_exact]
    rates_and_fids_single_click_leading = [single_click_rate_and_fidelity_leading_order(total_length=length,
                                                                                        asymmetry_parameter=asym,
                                                                                        q=q, dark_count_prob=pdc)
                                           for asym in asyms]
    rates_single_click_leading = [x[0] for x in rates_and_fids_single_click_leading]
    fids_single_click_leading = [x[1] for x in rates_and_fids_single_click_leading]
    rates_and_fids_double_click_exact = [double_click_rate_and_fidelity(total_length=length, asymmetry_parameter=asym,
                                                                        dark_count_prob=pdc)
                                         for asym in asyms]
    rates_double_click_exact = [x[0] for x in rates_and_fids_double_click_exact]
    fids_double_click_exact = [x[1] for x in rates_and_fids_double_click_exact]
    rates_and_fids_double_click_leading = [double_click_rate_and_fidelity_leading_order(total_length=length,
                                                                                        asymmetry_parameter=asym,
                                                                                        dark_count_prob=pdc)
                                           for asym in asyms]
    rates_double_click_leading = [x[0] for x in rates_and_fids_double_click_leading]
    fids_double_click_leading = [x[1] for x in rates_and_fids_double_click_leading]

    axs[0].plot(length_differences, rates_single_click_exact, label="single click exact", color="r", linestyle="-")
    axs[0].plot(length_differences, rates_single_click_leading, label="single click leading", color="r", linestyle=":")
    axs[0].plot(length_differences, rates_double_click_exact, label="double click exact", color="b", linestyle="-")
    axs[0].plot(length_differences, rates_double_click_leading, label="double click leading", color="b", linestyle=":")
    axs[0].set(ylabel="rate", xlabel=r"$\Delta L$ [km]")
    axs[1].plot(length_differences, fids_single_click_exact, label="single click exact", color="r", linestyle="-")
    axs[1].plot(length_differences, fids_single_click_leading, label="single click leading", color="r", linestyle=":")
    axs[1].plot(length_differences, fids_double_click_exact, label="double click exact", color="b", linestyle="-")
    axs[1].plot(length_differences, fids_double_click_leading, label="double click leading", color="b", linestyle=":")
    axs[1].set(ylabel="fidelity", xlabel=r"$\Delta L$ [km]")
    plt.legend()
    plt.tight_layout()
    plt.savefig("figures/imbalance_verification", dpi=480)