from asymmetry_code.midpoint_asymmetry.elementary_link import double_click_rate_and_fidelity, \
    double_click_rate_and_fidelity_calculated_directly, double_click_success_prob_and_fidelity, link_distances, \
    success_prob_to_rate, attenuation_efficiency, balanced_bright_state_params, \
    single_click_success_prob_and_fidelity, single_click_success_prob_calculated_directly, \
    balanced_bright_state_params_from_q, double_click_success_prob_and_fidelity_calculated_directly, \
    double_click_success_prob_and_fidelity_leading_order
import numpy as np


def test_double_click_rate_and_fidelity_calculated_directly():
    total_length = 100
    detector_efficiency = 0.8
    visibility = 0.9
    emission_fidelity = 0.95
    p_loss_length = 0.2
    speed_of_light = 2E+5
    for num_resolving in [True, False]:
        for asymmetry_parameter in [0., 0.25, 0.5, 0.75, 1.]:
            for dark_count_prob in [0., 0.01]:
                rate, fid = double_click_rate_and_fidelity(total_length=total_length,
                                                           asymmetry_parameter=asymmetry_parameter,
                                                           dark_count_prob=dark_count_prob,
                                                           detector_efficiency=detector_efficiency,
                                                           visibility=visibility,
                                                           emission_fidelity=emission_fidelity,
                                                           p_loss_length=p_loss_length,
                                                           speed_of_light=speed_of_light,
                                                           num_resolving=num_resolving
                                                           )
                rate_dir, fid_dir = \
                    double_click_rate_and_fidelity_calculated_directly(total_length=total_length,
                                                                       asymmetry_parameter=asymmetry_parameter,
                                                                       dark_count_prob=dark_count_prob,
                                                                       detector_efficiency=detector_efficiency,
                                                                       visibility=visibility,
                                                                       emission_fidelity=emission_fidelity,
                                                                       p_loss_length=p_loss_length,
                                                                       speed_of_light=speed_of_light,
                                                                       num_resolving=num_resolving)
                succ_prob, fid2 = double_click_success_prob_and_fidelity(total_length=total_length,
                                                                         asymmetry_parameter=asymmetry_parameter,
                                                                         dark_count_prob=dark_count_prob,
                                                                         detector_efficiency=detector_efficiency,
                                                                         visibility=visibility,
                                                                         emission_fidelity=emission_fidelity,
                                                                         p_loss_length=p_loss_length,
                                                                         num_resolving=num_resolving
                                                                         )
                succ_prob_dir, fid_dir2 = \
                    double_click_success_prob_and_fidelity_calculated_directly(total_length=total_length,
                                                                               asymmetry_parameter=asymmetry_parameter,
                                                                               dark_count_prob=dark_count_prob,
                                                                               detector_efficiency=detector_efficiency,
                                                                               visibility=visibility,
                                                                               emission_fidelity=emission_fidelity,
                                                                               p_loss_length=p_loss_length,
                                                                               num_resolving=num_resolving)
            assert np.isclose(rate_dir, rate)
            assert np.isclose(fid_dir, fid)
            assert np.isclose(succ_prob_dir, succ_prob)
            assert fid2 == fid
            assert fid_dir2 == fid_dir
            if dark_count_prob == 0.:
                succ_prob_leading_order, fid_leading_order = \
                    double_click_success_prob_and_fidelity_leading_order(total_length=total_length,
                                                                         asymmetry_parameter=asymmetry_parameter,
                                                                         dark_count_prob=dark_count_prob,
                                                                         detector_efficiency=detector_efficiency,
                                                                         visibility=visibility,
                                                                         emission_fidelity=emission_fidelity,
                                                                         p_loss_length=p_loss_length,
                                                                         num_resolving=num_resolving)
                assert np.isclose(succ_prob_leading_order, succ_prob)
                assert np.isclose(fid_leading_order, fid)


def test_single_click_success_prob_calculated_directly():
    total_length = 100
    dark_count_prob = 0.1
    detector_efficiency = 0.8
    visibility = 0.9
    p_loss_length = 0.2
    for num_resolving in [True, False]:
        for asymmetry_parameter in [0., 0.5, 1.]:
            for q in [1E-5, 1E-2, 1E-1]:
                succ_prob, _ = single_click_success_prob_and_fidelity(total_length=total_length,
                                                                      asymmetry_parameter=asymmetry_parameter,
                                                                      dark_count_prob=dark_count_prob,
                                                                      detector_efficiency=detector_efficiency,
                                                                      visibility=visibility,
                                                                      p_loss_length=p_loss_length,
                                                                      num_resolving=num_resolving,
                                                                      q=q
                                                                      )
                succ_prob_dir = single_click_success_prob_calculated_directly(dark_count_prob=dark_count_prob,
                                                                              visibility=visibility,
                                                                              num_resolving=num_resolving,
                                                                              q=q)
                assert np.isclose(succ_prob_dir, succ_prob)


def test_link_distances():
    for asymmetry_sign in [+1, -1]:
        l1, l2 = link_distances(total_length=100., asymmetry_parameter=0., asymmetry_sign=asymmetry_sign)
        assert l1 == l2 == 50.
    l1, l2 = link_distances(total_length=100., asymmetry_parameter=1., asymmetry_sign=+1)
    assert l1 == 0.
    assert l2 == 100.
    l1, l2 = link_distances(total_length=100., asymmetry_parameter=1., asymmetry_sign=-1)
    assert l1 == 100
    assert l2 == 0.


def test_success_prob_to_rate():
    assert success_prob_to_rate(success_prob=1., length_a=1., length_b=1., speed_of_light=1.) == .5
    assert success_prob_to_rate(success_prob=.5, length_a=1., length_b=1., speed_of_light=1.) == .25
    assert success_prob_to_rate(success_prob=1., length_a=100., length_b=1., speed_of_light=1.) == .005
    assert success_prob_to_rate(success_prob=1., length_a=1., length_b=100., speed_of_light=1.) == .005


def test_attenuation_efficiency():
    assert attenuation_efficiency(length=0.) == 1.
    assert attenuation_efficiency(length=1, p_loss_length=10) == 1 / 10


def test_balanced_bright_state_params():
    alpha_a = 0.1
    alpha_b = balanced_bright_state_params(length_a=1., length_b=1., alpha_a=alpha_a)
    assert alpha_a == alpha_b
    alpha_b = balanced_bright_state_params(length_a=10., length_b=1., alpha_a=alpha_a)
    assert alpha_a > alpha_b


def test_balanced_bright_state_params_from_q():
    q = 0.1
    length_a = 30
    length_b = 70
    detector_efficiency = .9
    p_loss_init_a = .1
    p_loss_init_b = .2
    p_a = attenuation_efficiency(length_a) * detector_efficiency * (1 - p_loss_init_a)
    p_b = attenuation_efficiency(length_b) * detector_efficiency * (1 - p_loss_init_b)
    alpha_a, alpha_b = balanced_bright_state_params_from_q(q=q, length_a=length_a, length_b=length_b,
                                                           detector_efficiency=detector_efficiency,
                                                           p_loss_init_a=p_loss_init_a,
                                                           p_loss_init_b=p_loss_init_b)
    assert np.isclose(alpha_a * p_a, q)
    assert np.isclose(alpha_b * p_b, q)
