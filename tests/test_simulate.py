from asymmetry_code.repeater_simulation.simulate import simulate, additional_packages
import os
import pickle


def test_state():
    # first clean up any lingering data in the test folder
    for filename in os.listdir("tests"):
        if filename[-7:] == ".pickle":
            os.remove("tests/" + filename)

    # run a simulation
    data = simulate(config_file="tests/test_configs/simulation_test.yaml", n_runs=10, save_path="tests",
                    extra_info="state_test_data", suppress_output=True)

    # check if the data complies with expectations
    assert data.varied_parameters == ["asymmetry"]
    assert data.number_of_nodes == 2
    assert data.number_of_results == 3 * 10  # 3 values of asymmetry, 10 runs
    assert data.baseline_parameters["speed_of_light"] == 200000
    assert data.baseline_parameters["attenuation_coefficient"] == .2
    assert data.baseline_parameters["coherence_time"] == 1.E10
    assert data.baseline_parameters["fidelity"] == .95
    assert data.baseline_parameters["efficiency"] == .99
    assert data.baseline_parameters["total_length"] == 300
    assert data.baseline_parameters["cutoff_time"] == 1.E9
    assert data.baseline_parameters["num_parallel_links"] == 2
    assert not data.baseline_parameters["spooled_fiber"]
    assert data.baseline_parameters["signs"] == "alternating"

    # retrieve the saved simulation data
    saved_data_filename = None
    for filename in os.listdir("tests"):
        if filename[:15] == "state_test_data":
            saved_data_filename = filename
    assert saved_data_filename is not None
    file_path = "tests/" + saved_data_filename
    assert os.path.isfile(file_path)
    with open(file_path, "rb") as file:
        data_from_saved_file = pickle.load(file)

    # check (in a lazy way) that the retrieved simulation data is the same object as the returned simulation data

    assert data.varied_parameters == data_from_saved_file.varied_parameters
    assert data.number_of_nodes == data_from_saved_file.number_of_nodes
    assert data.number_of_results == data_from_saved_file.number_of_results
    assert all(data.dataframe["midpoint_outcome_0"] == data_from_saved_file.dataframe["midpoint_outcome_0"])
    assert all(data.dataframe["generation_duration"] == data_from_saved_file.dataframe["generation_duration"])

    for package in additional_packages:
        assert package in data.packages

    # remove the saved data
    os.remove(file_path)
