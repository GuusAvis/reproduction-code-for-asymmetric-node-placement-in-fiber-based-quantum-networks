from asymmetry_code.midpoint_asymmetry.dispersion import group_velocity_dispersion_parameter, gaussian_photon_visibility_dispersion_correction, \
    gaussian_photon_visibility_without_dispersion, gaussian_photon_relative_dispersion_correction, \
    gaussian_photon_visibility, third_order_dispersion, speed_of_light_in_vacuum, lorentzian_photon_visibility, \
    lorentzian_photon_visibility_numerical, gaussian_photon_visibility_numerical
import numpy as np


def test_group_velocity_dispersion_parameter():
    beta_2 = group_velocity_dispersion_parameter(wavelength=1550, dispersion_coefficient=17)
    assert beta_2 < 0
    assert np.isclose(beta_2, -2.168E-26)  # calculated by hand / wolfram alpha


def test_third_order_dispersion():
    dispersion_slope = 0.058
    dispersion_slope_si_units = dispersion_slope * 1E3  # ps = 1E-12 s, nm^2 = 1E-18 m^2, km = 1E3 m
    wavelength = 1550
    wavelength_si_units = wavelength * 1E-9  # 1 nm = 1E-9 m
    beta_2 = group_velocity_dispersion_parameter(wavelength=wavelength, dispersion_coefficient=17)
    beta_3 = third_order_dispersion(wavelength=wavelength, dispersion_coefficient=17, dispersion_slope=dispersion_slope)
    # test against conversion equation, see page 114 in Fiber Optics by Fedor Mitschke
    assert np.isclose(dispersion_slope_si_units, ((2 * np.pi * speed_of_light_in_vacuum) ** 2 /
                                                  wavelength_si_units ** 4 * beta_3 + 4 * np.pi *
                                                  speed_of_light_in_vacuum / wavelength_si_units ** 3 * beta_2))


def test_gaussian_visibility_dispersion_correction():
    assert gaussian_photon_visibility_dispersion_correction(length_difference=0, time_sigma=1) == 0.


def test_gaussian_visibility_without_dispersion():
    assert np.isclose(gaussian_photon_visibility_without_dispersion(time_sigma=1, frequency_mismatch=0,
                                                                    time_offset=0),
                      1.)
    assert gaussian_photon_visibility_without_dispersion(time_sigma=1, frequency_mismatch=1, time_offset=0.) < 1.
    assert gaussian_photon_visibility_without_dispersion(time_sigma=1, frequency_mismatch=0., time_offset=.1) < 1.


def test_gaussian_relative_dispersion_correction():
    # when there is no frequency mismatch or time offset, the base visibility should be 1, and thus the relative
    # and absolute corrections are the same; adding such imperfections makes the relative one bigger than the absolute
    assert np.isclose(gaussian_photon_relative_dispersion_correction(length_difference=100, time_sigma=1,
                                                                     time_offset=0., frequency_mismatch=0.,
                                                                     dispersion_coefficient=17, dispersion_slope=0.058,
                                                                     wavelength=1550),
                      gaussian_photon_visibility_dispersion_correction(length_difference=100, time_sigma=1,
                                                                       time_offset=0., frequency_mismatch=0.,
                                                                       dispersion_coefficient=17,
                                                                       dispersion_slope=0.058, wavelength=1550)
                      )
    assert abs(gaussian_photon_relative_dispersion_correction(length_difference=100, time_sigma=1,
                                                              time_offset=.1, frequency_mismatch=1,
                                                              dispersion_coefficient=17, dispersion_slope=0.058,
                                                              wavelength=1550)) \
           > \
           abs(gaussian_photon_visibility_dispersion_correction(length_difference=100, time_sigma=1,
                                                                time_offset=.1, frequency_mismatch=1,
                                                                dispersion_coefficient=17, dispersion_slope=0.058,
                                                                wavelength=1550))


def test_gaussian_visibility():
    assert np.isclose(gaussian_photon_visibility(length_difference=0., time_sigma=1, time_offset=0.,
                                                 frequency_mismatch=0., dispersion_coefficient=1000,
                                                 dispersion_slope=1000, wavelength=1550),
                      1.)


def test_gaussian_visibility_numerical():
    assert np.isclose(gaussian_photon_visibility_numerical(length_difference=0., time_sigma=1, time_offset=0.,
                                                           frequency_mismatch=0., dispersion_coefficient=1000,
                                                           dispersion_slope=1000, wavelength=1550)[0],
                      1.)
    assert np.isclose(gaussian_photon_visibility_numerical(length_difference=100, time_sigma=1000,
                                                           dispersion_coefficient=0.)[0],
                      1.)
    assert gaussian_photon_visibility_numerical(length_difference=1E10, time_sigma=1., epsabs=1E-1, epsrel=1E-1)[0] < .1
    assert gaussian_photon_visibility_numerical(length_difference=0., time_sigma=1., time_offset=10., epsabs=1E-1,
                                                epsrel=1E-1)[0] < .1
    assert gaussian_photon_visibility_numerical(length_difference=0., time_sigma=1., frequency_mismatch=100,
                                                epsabs=1E-1, epsrel=1E-1)[0] < 0.5


def test_lorentzian_visibility():
    assert lorentzian_photon_visibility(length_difference=0, tau=10000) == 1.
    assert lorentzian_photon_visibility(length_difference=100, tau=10000, dispersion_coefficient=0.) == 1.
    assert np.isclose(lorentzian_photon_visibility(length_difference=100, tau=1E13), 1.)
    assert lorentzian_photon_visibility(length_difference=1E10, tau=1) < 0.1


def test_lorentzian_visibility_numerical():
    assert np.isclose(lorentzian_photon_visibility_numerical(length_difference=0., tau=1000)[0], 1.)
    assert np.isclose(
        lorentzian_photon_visibility_numerical(length_difference=100, tau=1000, dispersion_coefficient=0.)[0],
        1.)
    assert np.isclose(lorentzian_photon_visibility_numerical(length_difference=100, tau=1E13)[0], 1.)
    assert lorentzian_photon_visibility_numerical(length_difference=1E10, tau=1, epsabs=1E-1)[0] < 0.1
    assert lorentzian_photon_visibility_numerical(length_difference=0., tau=1., time_offset=10., epsabs=1E-1,
                                                  epsrel=1E-1)[0] < 0.5
    assert lorentzian_photon_visibility_numerical(length_difference=0., tau=1., frequency_mismatch=100, epsabs=1E-1,
                                                  epsrel=1E-1)[0] < 0.5
