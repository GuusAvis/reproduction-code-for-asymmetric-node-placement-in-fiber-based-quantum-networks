from netsquid_netconf.builder import ComponentBuilder
from netsquid_netconf.netconf import netconf_generator

from asymmetry_code.repeater_simulation.chain_builder import NodeCoordinateCalculator, ChainBuilder
import numpy as np
import unittest
import os

from netsquid.nodes.connections import Connection
from netsquid.nodes.node import Node


class SimpleConnection(Connection):
    def __init__(self, name, length):
        super().__init__(name=name)
        self.length = length


class SimpleNode(Node):
    def __init__(self, name, end_node, num_positions, **kwargs):
        super().__init__(name=name, **kwargs)
        self.end_node = end_node
        self.num_positions = num_positions


class TestNodeCoordinateCalculator(unittest.TestCase):
    """Test :class:`NodeCoordinateCalculator`."""

    def setUp(self) -> None:
        self.total_length = 100
        self.num_nodes = 11

    def test_symmetric(self):
        """Test a symmetric, regular chain is obtained when asymmetry parameter is zero."""
        asymmetry_parameter = 0
        node_coordinate_calculator = NodeCoordinateCalculator(total_length=self.total_length,
                                                              num_nodes=self.num_nodes,
                                                              asymmetry_parameter=asymmetry_parameter)
        assert node_coordinate_calculator.asymmetry_parameter == asymmetry_parameter
        signs = [+1] * (self.num_nodes - 2)
        node_coordinate_calculator.signs = signs
        assert node_coordinate_calculator.signs == signs
        coordinates = node_coordinate_calculator.coordinates
        num_segments = self.num_nodes - 1
        for i in range(len(coordinates) - 1):
            assert coordinates[i + 1] == coordinates[i] + self.total_length / num_segments

    def test_two_to_one(self):
        """Assert that when asymmetry parameter = 1 / 3, the ratio between segments is 2 : 1."""
        asymmetry_parameter = 1 / 3
        node_coordinate_calculator = NodeCoordinateCalculator(total_length=self.total_length,
                                                              num_nodes=self.num_nodes,
                                                              asymmetry_parameter=asymmetry_parameter)
        node_coordinate_calculator.signs = [+1] * (self.num_nodes - 2)
        coordinates = node_coordinate_calculator.coordinates
        for i in range(len(coordinates) - 2):
            assert np.isclose(2 * (coordinates[i + 2] - coordinates[i + 1]),
                              coordinates[i + 1] - coordinates[i])

    def test_large_asymmetry(self):
        """Check that if asymmetry is large and alternating, groups of two close-together nodes are formed."""
        asymmetry_parameter = 1 - 1E-2
        node_coordinate_calculator = NodeCoordinateCalculator(total_length=self.total_length,
                                                              num_nodes=self.num_nodes,
                                                              asymmetry_parameter=asymmetry_parameter)
        node_coordinate_calculator.set_signs_alternating()
        coordinates = node_coordinate_calculator.coordinates
        num_segments = self.num_nodes - 1
        for i in range(len(coordinates) - 1):
            # Nodes are alternatingly far apart and close together.
            # This results in effectively only half the number of total segments, each of equal length.
            # Between all segments are two nodes lying close together.
            if i % 2 == 1:
                assert np.isclose(coordinates[i + 1], coordinates[i], atol=0.2)
            else:
                assert np.isclose(coordinates[i+1] - coordinates[i], 2 * self.total_length / num_segments, atol=0.2)

    def test_spooled_fiber(self):
        node_coordinate_calculator_symmetric = NodeCoordinateCalculator(total_length=self.total_length,
                                                                        num_nodes=self.num_nodes,
                                                                        asymmetry_parameter=0.,
                                                                        spooled_fiber=True)
        node_coordinate_calculator_symmetric.set_signs_randomly()
        symmetric_coordinates = node_coordinate_calculator_symmetric.coordinates
        assert symmetric_coordinates[-1] == self.total_length
        for i in range(self.num_nodes):
            assert symmetric_coordinates[i] == self.total_length / (self.num_nodes - 1) * i
        asymmetry_parameter = .5
        without_spooled_fiber = NodeCoordinateCalculator(total_length=self.total_length,
                                                         num_nodes=self.num_nodes,
                                                         asymmetry_parameter=asymmetry_parameter,
                                                         spooled_fiber=False)
        without_spooled_fiber.set_signs_alternating()
        distances_without = [without_spooled_fiber.coordinates[i + 1] - without_spooled_fiber.coordinates[i]
                             for i in range(self.num_nodes - 1)]
        largest_distance = max(distances_without)
        assert largest_distance > self.total_length / (self.num_nodes - 1)
        with_spooled_fiber = NodeCoordinateCalculator(total_length=self.total_length,
                                                      num_nodes=self.num_nodes,
                                                      asymmetry_parameter=asymmetry_parameter,
                                                      spooled_fiber=True)
        with_spooled_fiber.set_signs_alternating()
        for i in range(self.num_nodes):
            assert with_spooled_fiber.coordinates[i] == largest_distance * i
        assert with_spooled_fiber.coordinates[-1] > self.total_length


def test_chain_builder():
    ComponentBuilder.add_type(name="simple_connection", new_type=SimpleConnection)
    ComponentBuilder.add_type(name="simple_node", new_type=SimpleNode)
    generator = netconf_generator("tests/test_configs/chain_builder_test.yaml",
                                  extra_builders=[ChainBuilder])
    output_dict, _ = next(generator)
    network = output_dict["chain"]["network"]
    assert len(network.nodes) == 11
    for conn in network.connections.values():
        assert isinstance(conn, SimpleConnection)
    total_length = 0.
    for i in range(9):
        node1 = network.nodes[f"node_{i}"]
        node2 = network.nodes[f"node_{i + 1}"]
        node3 = network.nodes[f"node_{i + 2}"]

        # check whether only the end nodes are end nodes
        if i == 0:
            assert node1.end_node
            assert node1.num_positions == 1
        else:
            assert not node1.end_node
            assert node1.num_positions == 2
        if i == 8:
            assert node3.end_node
            assert node3.num_positions == 1
        else:
            assert not node3.end_node
            assert node3.num_positions == 2
        assert not node2.end_node
        assert node2.num_positions == 2

        cl_conn1 = network.get_connection(node1, node2, label="classical_connection")
        cl_conn2 = network.get_connection(node2, node3, label="classical_connection")
        ent_conn1 = network.get_connection(node1, node2, label="entanglement_connection")
        ent_conn2 = network.get_connection(node2, node3, label="entanglement_connection")
        assert cl_conn1.length == ent_conn1.length
        assert cl_conn2.length == ent_conn2.length
        assert np.isclose(cl_conn1.length, 2 * cl_conn2.length)
        total_length += cl_conn1.length
        if i == 8:
            total_length += cl_conn2.length
    assert total_length == 100.
    draw_filename = "test_chain_figure.png"  # this file should be generated for draw = test_chain_figure
    assert os.path.exists(draw_filename)
    os.remove(draw_filename)
    config_filename = "test_config_file.yml"  # this file should be generated for save_config = test_config_file
    assert os.path.exists(config_filename)
    os.remove(config_filename)

    # spooled fiber
    generator = netconf_generator("tests/test_configs/chain_builder_test_with_spooled_fiber.yaml",
                                  extra_builders=[ChainBuilder])
    output_dict, _ = next(generator)
    network = output_dict["chain"]["network"]
    total_length = 0.
    for i in range(9):
        node1 = network.nodes[f"node_{i}"]
        node2 = network.nodes[f"node_{i + 1}"]
        node3 = network.nodes[f"node_{i + 2}"]
        ent_conn1 = network.get_connection(node1, node2, label="entanglement_connection")
        ent_conn2 = network.get_connection(node2, node3, label="entanglement_connection")
        assert np.isclose(ent_conn1.length, ent_conn2.length)  # symmetric
        total_length += ent_conn1.length
        if i == 8:
            total_length += ent_conn2.length
    assert total_length > 100.
