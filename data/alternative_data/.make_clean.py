import os

if __name__ == "__main__":
    # This script is assumed to be located in the alternative data directory.
    alternative_data_dir = os.path.dirname(os.path.abspath(__file__))
    for root, subdirs, files in os.walk(alternative_data_dir):
        for file in files:
            if file[-4:] == ".csv" or file[-4:] == ".png" or file == "processed_data.pickle":
                path = f"{root}/{file}"
                os.remove(path)
                print(f"Removed file {path}.")
