from asymmetry.plot_qkd import plot_qkd_comparison_from_files, save_plot
from netsquid_simulationtools.repchain_data_process import process_data
import os

def make_comparison(path, name):
    fig, axs = plot_qkd_comparison_from_files(filename1=f"{path}/no_spooled_fiber/output.csv",
                                              filename2=f"{path}/spooled_fiber/output.csv",
                                              label1="asymmetric", label2="extended fiber")
    # axs[0].set_xlim([0., .5])
    # axs[0].set_xticks([0.1 * x for x in range(6)])
    save_plot(f"{path}/{name}")

if __name__ == "__main__":
    # This script is assumed to be located in the alternative data directory.
    alternative_data_dir = os.path.dirname(os.path.abspath(__file__))
    for item in os.listdir(alternative_data_dir):
        path = f"{alternative_data_dir}/{item}"
        if os.path.isdir(path):
            make_comparison(path=path, name=item)
            print(f"Created figure in {path}.")

