from asymmetry.plot_qkd import plot_qkd_comparison_from_files, save_plot
from netsquid_simulationtools.repchain_data_process import process_data
import os

def make_process(path):
    for p in [path + "/no_spooled_fiber", path + "/spooled_fiber"]:
        if not os.path.isfile(p + "/output.csv"):
            print(p)
            process_data(raw_data_dir=p + "/raw_data", process_bb84=True, process_teleportation=False,
                        process_fidelity=False, plot_processed_data=False, output=p + "/processed_data.pickle",
                        csv_output_filename=p + "/output.csv")

if __name__ == "__main__":
    # This script is assumed to be located in the alternative data directory.
    alternative_data_dir = os.path.dirname(os.path.abspath(__file__))
    for item in os.listdir(alternative_data_dir):
        path = f"{alternative_data_dir}/{item}"
        if os.path.isdir(path):
            make_process(path=path)
            print(f"Processed data in {path}.")

