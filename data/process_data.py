from netsquid_simulationtools.repchain_data_process import process_data


if __name__ == "__main__":
    paths = ["data/spooled_fiber", "data/no_spooled_fiber"]
    for path in paths:
        process_data(raw_data_dir=path + "/raw_data", process_bb84=True, process_teleportation=False,
                     process_fidelity=False, plot_processed_data=False, output=path + "/processed_data.pickle",
                     csv_output_filename=path + "/output.csv")
