SOURCEDIR   = asymmetry_code
TESTDIR     = tests
PYTHON      = python3
PIP_FLAGS   = --extra-index-url=https://${NETSQUIDPYPI_USER}:${NETSQUIDPYPI_PWD}@pypi.netsquid.org

requirements: _check_variables
	@cat requirements.txt | xargs -n 1 $(PYTHON) -m pip install ${PIP_FLAGS}
tests:
	@$(PYTHON) -m pytest --cov=${SOURCEDIR} ${TESTDIR} 
lint:
	@$(PYTHON) -m flake8 ${SOURCEDIR}
_check_variables:
ifndef NETSQUIDPYPI_USER
	$(error Set the environment variable NETSQUIDPYPI_USER before uploading)
endif
ifndef NETSQUIDPYPI_PWD
	$(error Set the environment variable NETSQUIDPYPI_PWD before uploading)
endif
process:
	@$(PYTHON) data/process_data.py
figs:
	sh ./.make_all_figures.sh

.PHONY: requirements tests lint process figs _check_variables
